<?php
require_once 'functions.php';
$ime='';
$prezime='';
$zvanje='';
if(isset($_POST['potvrdi'])){
    if($_POST['ime']!='' && $_POST['prezime']!='' && $_POST['zvanje']!=''){
        createProfessor();
        //echo "<script>window.location.href='uspesno_kreiranje_profesora.php'</script>";
    }
    else{
        if(empty($_POST['ime'])){
            $ime="Polje ne sme biti prazno";
        }
        if(empty($_POST['prezime'])){
            $prezime='Polje ne sme biti prazno';
        }
        if(empty($_POST['zvanje'])){
            $zvanje="Polje ne sme biti prazno";
        }
    
    }
}

?>
<head>        
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="icon" type="image/gif" href="Images/masm.png" />
         <link rel="shortcut icon" type="image/gif" href="Images/masm.png" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
    </head>
<body id='parent' style='background-image:url("Images/mybg.png");'>
    <div class='container'>
        <h1 class='jumbotron'>Kreiraj novog profesora</h1>  
                     
        <div  class='col-xs-12 col-sm-12 col-md-12'>
           
            
                <form action='' method='post'>    

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                            <label for="usr">Ime:</label>
                            <input type="text" placeholder="<?php echo $ime;?>" name='ime' class="form-control" id="usr" >
                        
                    </div>

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                            <label for="usr">Prezime:</label>
                            <input type="text" placeholder="<?php echo $prezime;?>" name='prezime' class="form-control" id="usr" >
                       
                    </div>

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                       
                            <label for="usr">Zvanje:</label>
                            <input type="text" placeholder="<?php echo $zvanje;?>" name='zvanje' class="form-control" id="usr" >
                        
                    </div>

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                       <br/><input  type='submit' class='btn btn-success btn-lg' name='potvrdi' value='Potvrdi'>
                        
                    </div>

                </form>
            </div>
        </div><br />
        
        
   

    <script src='src/scripts.js'></script>
    <script src='src/animation.js'></script>
</body>