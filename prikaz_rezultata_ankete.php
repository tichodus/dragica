<?php
    require_once './functions.php';
    require_once 'style.html';
    session_start();
    $pred = $_SESSION['predmet'];
    echo "<h1 class='container jumbotron'>Rezultati ankete za iz predmeta $pred->naziv</h1>";
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dragica</title>
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	    <script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </head>
 
    <?php
            $br=0;
            $pit1=0;
            $pit2=0;
            $pit3=0;
            $pit4=0;
            $pit5=0;
            $pit6=0;
            $pit7=0;
            $pit8=0;
            $pit9=0;
            $pit10=0;
            $pit11=0;
            $pit12=0;
            $pit13=0;
            $pit14=0;
            $pit15=0;
        if($nizpod = podaci_za_graficki_prikaz_ankete($pred->id))
        {          
            foreach ($nizpod as $pod)
            {
                $pit1=$pit1 + $pod->question1;
                $pit2=$pit2 + $pod->question2;
                $pit3=$pit3 + $pod->question3;
                $pit4=$pit4 + $pod->question4;
                $pit5=$pit5 + $pod->question5;
                $pit6=$pit6 + $pod->question6;
                $pit7=$pit7 + $pod->question7;
                $pit8=$pit8 + $pod->question8;
                $pit9=$pit9 + $pod->question9;
                $pit10=$pit10 + $pod->question10;
                $pit11=$pit11 + $pod->question11;
                $pit12=$pit12 + $pod->question12;
                $pit13=$pit13 + $pod->question13;
                $pit14=$pit14 + $pod->question14;
                $pit15=$pit15 + $pod->question15;
                $br++;
            }
        }
        else
            {
                $br = 1;
            }
        /*$dataPoints = {
            {y : $pit1/$br, label : "Predznanje koje sam imao/la je bilo dovoljno za pracenje nastave"},
            {y : $pit2/$br, label : "Studenti su na vreme upoznati sa sadrzajem/programom predmeta i nacinom ocenjivanja"},
            {y : $pit3/$br, label : "Nastavne celine su dobro osmisljenje"},
            {y : $pit4/$br, label : "Oblici izvodjenja nastave(predavanja, vezbe, praksa...) odgovaraju sadrzaju predmeta"},
            {y : $pit5/$br, label : "Oprema i tehnicka podrska odgovaraju oblicima nastave"},
            {y : $pit6/$br, label : "Nastavni sadrzaj omogucuje sticanje adekvatnih nivoa znanja"},
            {y : $pit7/$br, label : "Opterecenje studenata na predmetu je u skladu sa dodeljenim ESPB bodovima"},
            {y : $pit8/$br, label : "Nastava je interaktivna i ukljucuje primere iz prakse."},
            {y : $pit9/$br, label : "Nastavnik i saradnik primenjuju literaturu navedenu u specifikaciji predmeta"},
            {y : $pit10/$br, label : "Literaturom je obuhvacena celokupna ispitna materija"},
            {y : $pit11/$br, label : "Nastavni materijal je dostupan"}, 
            {y : $pit12/$br, label : "Nastavni materijal je jasan, razumljiv i tehnicki dobro uradjen"},
            {y : $pit13/$br, label : "Rad studenta se prati i ocenjuje tokom nastave"},
            {y : $pit14/$br, label : "Ocenjivanje nastavnika tokom nastave i na zavrsonm ispitu je profesionalno i u skladu sa definisanim kriterijumima"},
            {y : $pit15/$br, label : "Nastavnik kroz ocenjivanje vrednuje razumevanje i sposobnost primene znanja"}
        );*/
    ?>
 
    <body style='background-image:url("Images/mybg.png");'>
        
        <div id="chartContainer" class='container'>   
            
        </div>
        
        <script type="text/javascript">
 /*
            $(function () {
                var chart = new CanvasJS.Chart("chartContainer", {
                    theme: "theme2",
                    animationEnabled: true,
                    title: {
                        text: "Odgovori vezani za predmet"
                    },
                    data: [
                    {
                        type: "bar",                
                        dataPoints: <?php //echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                    }
                    ]
                });
                console.log(chart);
                chart.render();
                console.log(chart.axisY[0].maximum);
                chart.axisY[0]
            });*/
        </script>
    <script type="text/javascript"> 
        window.onload = function(){
        var chart = new CanvasJS.Chart("chartContainer",
    {
        theme: "theme2",
     
      axisY: {
        maximum: 5
      },
      data: [
      {
        type: "bar",
        dataPoints: [
            {y : <?php echo $pit1/$br?>, label : "Predznanje koje sam imao/la je bilo dovoljno za pracenje nastave"},
            {y : <?php echo $pit2/$br?>, label : "Studenti su na vreme upoznati sa sadrzajem/programom predmeta i nacinom ocenjivanja"},
            {y : <?php echo $pit3/$br?>, label : "Nastavne celine su dobro osmisljenje"},
            {y : <?php echo $pit4/$br?>, label : "Oblici izvodjenja nastave(predavanja, vezbe, praksa...) odgovaraju sadrzaju predmeta"},
            {y : <?php echo $pit5/$br?>, label : "Oprema i tehnicka podrska odgovaraju oblicima nastave"},
            {y : <?php echo $pit6/$br?>, label : "Nastavni sadrzaj omogucuje sticanje adekvatnih nivoa znanja"},
            {y : <?php echo $pit7/$br?>, label : "Opterecenje studenata na predmetu je u skladu sa dodeljenim ESPB bodovima"},
            {y : <?php echo $pit8/$br?>, label : "Nastava je interaktivna i ukljucuje primere iz prakse."},
            {y : <?php echo $pit9/$br?>, label : "Nastavnik i saradnik primenjuju literaturu navedenu u specifikaciji predmeta"},
            {y : <?php echo $pit10/$br?>, label : "Literaturom je obuhvacena celokupna ispitna materija"},
            {y : <?php echo $pit11/$br?>, label : "Nastavni materijal je dostupan"}, 
            {y : <?php echo $pit12/$br?>, label : "Nastavni materijal je jasan, razumljiv i tehnicki dobro uradjen"},
            {y : <?php echo $pit13/$br?>, label : "Rad studenta se prati i ocenjuje tokom nastave"},
            {y : <?php echo $pit14/$br?>, label : "Ocenjivanje nastavnika tokom nastave i na zavrsonm ispitu je profesionalno i u skladu sa definisanim kriterijumima"},
            {y : <?php echo $pit15/$br?>, label : "Nastavnik kroz ocenjivanje vrednuje razumevanje i sposobnost primene znanja"}
            //{y: 957, label: "USA"}
        ]
      }
      ]
    });

chart.render();
 $(document).ready(function()
 {
     var display = $("a").css("display");
     if(display!= "none")
     {
         $("a").attr("style", "display:none");
     }
 });
}
    </script>
    </body>
 
</html>