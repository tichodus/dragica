<?php
require_once './functions.php';
require_once './classes.php';
require_once './style.html';
    session_start();
    $kor = $_SESSION['korisnik'];
    
    if(jeDragica($kor->korisnicko_ime))
    {
        $profesori = vrati_sve_profesore();
        $javasob = json_encode($profesori);
        ?>
        <body id='parent' style='background-image:url("Images/mybg.png");'>
        <div class='container'>
        <h1 class='jumbotron'>Pretrazite profesora za brisanje</h1>
        <div class='col-xs-12 col-sm-12 col-md-12'>
        <form action="obrisi_prof.php" method="POST">
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <input id="iip" class='form-control' type="search"/><br />
            <input type="button" class='btn btn-success btn-lg' value="Traži" id="trazi">
        </div>
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <div class='table-responsive'>
            <table class='table' id='table'>
                <thead>
                    <tr>
                        <th>Ime</th>
                        <th>Zvanje</th>
                        <th>Korisnicko ime</th>
                    </tr>
                </thead>
                <tbody id='tbody'>

                </tbody>
            </table>
            </div>
            <input type="submit" class='btn btn-success btn-lg' value="Obriši" id="obebtn"/>
            </div>
        </form>
        </div>
        </div><br/>
<script>
    $("#trazi").click( function()
    {
        document.getElementById('tbody').innerHTML="";
        var vred = $("#iip").val().toLowerCase();
        var profesori = <?php echo $javasob; ?>;
        var provera;
        for(var i=0;i<profesori.length;i++)
        {
            if((provera=profesori[i].ime.toLowerCase().search(vred))!=-1)
            {
                document.getElementById('tbody').innerHTML+='<tr><td>'+profesori[i].ime+'</td>'+'<td>'+profesori[i].zvanje+'</td>'+'<td>'+profesori[i].korisnicko_ime+'</td><td><input type="checkbox" name="check[]" value="'+i+'"></td></tr>';      
            }
        }
    });
</script>
        
    <?php
    
    }
    else
    {
        echo "Nemate privilegije za ovu akciju!";
    }
    ?>
    <script src='src/animation.js'></script>
    </body>