<?php
    
    require_once 'style.html';
    require_once 'functions.php';
    session_start();
    $semestar_po_redu = $_SESSION['korisnik']->id_semestra + 1;
    $korisnik=$_SESSION['korisnik'];
    $predmeti = vrati_obavezne_predmete();
    $izborni_predmeti = vrati_izborne_predmete();
    $ok = false;
    if(is_array($izborni_predmeti)){
    if(isset($_GET['izaberi'])){     
        for($i=0;$i<count($izborni_predmeti);$i++){
            if(isset($_GET[$i])){
              ubaci_predmet($_SESSION['korisnik'],$_GET[$i]);
              $ok = true;
            }
        }
        if(is_array($predmeti) && $ok){
            foreach($predmeti as $predmet)
              ubaci_predmet($_SESSION['korisnik'],$predmet->id);
        }
        else
            echo "<label class='alert alert-danger'>Morate selektovati makar jedan predmet.</label>";
        
        if($ok){
        povecajSemestar($_SESSION['korisnik']);
        $korisnik = vratiStudenta($korisnik->index);
        $_SESSION['korisnik']=$korisnik;
        $_SESSION['overa']=false;       
        echo "<script>window.top.location.href='student.php';</script>"; // ovde cemo staviti pocetnu stranuu studenta koja ce biti ucitana u iframe
        }  
    } 
    }
    else
    if(isset($_GET['izaberi'])){
        if(is_array($predmeti)){
            foreach($predmeti as $predmet)
              ubaci_predmet($_SESSION['korisnik'],$predmet->id);
        povecajSemestar($_SESSION['korisnik']);
        $korisnik = vratiStudenta($korisnik->index);
        $_SESSION['korisnik']=$korisnik;
        $_SESSION['overa']=false;       
        echo "<script>window.top.location.href='student.php';</script>";
    }
    }
    
?>
<head>
    <meta charset='utf8'>
</head>
<body style='background-image:url("Images/mybg.png");'>
    <div id='parent' class='container' >
        <?php
            if(isFirstYear($_SESSION['korisnik']))
                require_once 'izbor_smera.php';
        ?>
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <h1 class='jumbotron'>
                Lista obaveznih predmeta u narednom semestru
            </h1>
        </div>
        <?php
            if(isFirstYear($_SESSION['korisnik']))
                require_once 'izbor_smera.php';
        ?>
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <table class='table'>
                <thead>
                    <tr>
                        <th><label>Predmet</label></th>
                        <th><label>ESPB</label></th>
                        <th><label>Profesor</label></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(is_array($predmeti))
                        foreach($predmeti as $predmet){
                            echo '<tr><td>'.$predmet->naziv.'</td>'.
                            '<td>'.$predmet->espb.'</td>'.
                            '<td>'.$predmet->ime_profesora.'</td></tr>';
                        }

                    ?>
                </tbody>
            </table>
        </div>
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <h1 class='jumbotron'>
                Lista izbornih predmeta u narednom semestru
            </h1>
        </div> 
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <form action='' method='GET'>
                <table class='table'>
                    <thead>
                        <tr>
                            <th><label>Predmet</label></th>
                            <th><label>ESPB</label></th>
                            <th><label>Profesor</label></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i = 0;
                            $red = 0;
                            if(is_array($izborni_predmeti)){
                                echo '<tr><td>'.$izborni_predmeti[0]->naziv.'</td>'.
                                '<td>'.$$izborni_predmeti[0]->espb.'</td>'.
                                '<td>'.$$izborni_predmeti[0]->ime_profesora.'</td>'.
                                "<td>"."<input type='checkbox' checked name='$i' value='$predmet->id'/>"."</td></tr>";
                                $i++;
                            for($i=1;$i<count($izborni_predmeti);$i++){
                                echo '<tr><td>'.$predmet->naziv.'</td>'.
                                '<td>'.$$izborni_predmeti[i]->espb.'</td>'.
                                '<td>'.$$izborni_predmeti[i]->ime_profesora.'</td>'.
                                "<td>"."<input type='checkbox' name='$i' value='$predmet->id'/>"."</td></tr>";
                                $i++;
                            }
                            }
                        ?>
                    </tbody>
                </table>
                <input type='submit' value='Izaberi predmete' name='izaberi' class='btn btn-success btn-lg' />
            </form>
        </div>
        
    </div>
<script src='src/animation.js'></script>

</body>

