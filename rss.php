<?php
/* 
 ======================================================================
 lastRSS usage DEMO 1
 ----------------------------------------------------------------------
 This example shows, how to
 	- create lastRSS object
	- set transparent cache
	- get RSS file from URL
	- show result in array structure
 ======================================================================
*//*
require_once 'navigation-student.html';
// include lastRSS
include "./lastRSS.php";

// Create lastRSS object
$rss = new lastRSS;

// Set cache dir and cache time limit (1200 seconds)
// (don't forget to chmod cahce dir to 777 to allow writing)
$rss->cache_dir = '';
$rss->cache_time = 0;
$rss->cp = 'UTF-8';
$rss->date_format = 'l';

// Try to load and parse RSS file of Slashdot.org
$rssurl = 'https://www.npao.ni.ac.rs/doktoranti-van-univerziteta?format=feed';

if ($rs = $rss->get($rssurl)) {
    $pao=$rs['items'];
    foreach($pao as $el)
    {
        $autor = $el['title'];
        $linka = $el['link'];
        $danpub = $el['pubDate'];
        echo "<p> Autor: $autor</p>";
        echo "<a href='$linka'> Link autora</a>";
        echo "<p> Dan publikacije: $danpub</p>";
    }
}
else {
	echo "Error: It's not possible to get $rssurl...";
}

*/
?>

<?php
//get the q parameter from URL
$xml = 'https://www.npao.ni.ac.rs/doktoranti-van-univerziteta?format=feed';

$xmlDoc = new DOMDocument();
$xmlDoc->load($xml);

//get elements from "<channel>"
$channel=$xmlDoc->getElementsByTagName('channel')->item(0);
$channel_title = $channel->getElementsByTagName('title')
->item(0)->childNodes->item(0)->nodeValue;
$channel_link = $channel->getElementsByTagName('link')
->item(0)->childNodes->item(0)->nodeValue;
$channel_desc = $channel->getElementsByTagName('description')
->item(0)->childNodes->item(0)->nodeValue;

//output elements from "<channel>"
echo("<br>");
echo("<label>".$channel_desc . "</label>");



//get and output "<item>" elements
$x=$xmlDoc->getElementsByTagName('item');
for ($i=0; $i<=2; $i++) {
  $item_title=$x->item($i)->getElementsByTagName('title')
  ->item(0)->childNodes->item(0)->nodeValue;
  $item_link=$x->item($i)->getElementsByTagName('link')
  ->item(0)->childNodes->item(0)->nodeValue;
  $item_desc=$x->item($i)->getElementsByTagName('description')
  ->item(0)->childNodes->item(0)->nodeValue;
  echo ("<p><a href='" . $item_link
  . "'>" . $item_title . "</a>");
  echo ("<br>");
  echo ($item_desc . "</p>");
  
}
echo("<br /><br /><p><a href='" . $channel_link
  . "'>" . $channel_title . "</a>");
?>

<body style="background-image:url('Images/mybg.png');" class=''>

</body>