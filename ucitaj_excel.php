<?php
require_once './style.html';


?>
<html>
    
    <head>
        <?php
            require_once './functions.php';
            session_start();
            
            $prof = $_SESSION['korisnik'];       
            $niz = vrati_neocenjene_predmete($prof->id);
        ?>
    </head>
    <body style="background-image:url('Images/mybg.png');" class=''>
        <div class='container'>
        
        <form action='exread.php' method ='post' enctype="multipart/form-data">
            <h1 class='jumbotron'>Dodavanje fajla sa ocenama</h1>
        <p><h2>Izaberite predmet za koji dajete ocene</h2></p>
        <select class='form-control' name="predmeti">
            <?php
                $i = 0;
                if($niz)
                {
                    foreach($niz as $el)
                    {
                        echo "<option  id='$i' value='$el->id'>$el->naziv</option>";
                        $i++;
                    }
                }
                else
                {
                    echo "<option class='form-control' id=$i value=none>None</option>";
                }
            ?>
        </select>
            <p>
            <div>
                <input type="hidden" name="MAX_FILE_SIZE" value="20000000">
                <label class="btn btn-default btn-file">
                    Ucitaj fajl<input class='' name="fajl_input" type="file" style='display:none;' id="fajl_input">
                </label>
            </div>
            </p>
            <p><input type ='submit' class='btn btn-default btn-lg' name ='dodaj' value='Dodaj'/></p>
        </form>
      
        
        </div>
    </body>
</html>