-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2017 at 12:50 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dragica`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `password` varchar(30) NOT NULL,
  `korisnicko_ime` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `anketa`
--

CREATE TABLE `anketa` (
  `id_ankete` int(11) NOT NULL,
  `naziv` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `anketa`
--

INSERT INTO `anketa` (`id_ankete`, `naziv`) VALUES
(1, 'Upis na fakultet'),
(2, 'Izmene');

-- --------------------------------------------------------

--
-- Table structure for table `drzi`
--

CREATE TABLE `drzi` (
  `id` int(11) NOT NULL,
  `id_predmeta` int(11) DEFAULT NULL,
  `id_profesora` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `odgovori`
--

CREATE TABLE `odgovori` (
  `id_pitanja` int(11) NOT NULL,
  `id_studenta` int(11) NOT NULL,
  `id_odgovora` int(11) NOT NULL,
  `odgovor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pitanja`
--

CREATE TABLE `pitanja` (
  `id_pitanja` int(11) NOT NULL,
  `pitanje` varchar(300) NOT NULL,
  `id_ankete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pitanja`
--

INSERT INTO `pitanja` (`id_pitanja`, `pitanje`, `id_ankete`) VALUES
(1, 'Dosije broj:', 1),
(2, 'Semestar po redu:', 1),
(5, 'Semestar po redu:', 2),
(6, 'Semestar:', 1),
(7, 'Semestar:', 2),
(8, 'Skolska godina:', 1),
(9, 'Skolska godina:', 2),
(10, 'Finansiranje:', 1),
(11, 'Finansiranje:', 2),
(12, 'Prezime, ime jednog od roditelja, ime studenta:', 1),
(13, 'JMBG:', 1),
(14, 'Pol:', 1),
(15, 'Datum rodjenja:', 1),
(16, 'Mesto rodjenja, opstina, republika:', 1),
(17, 'Mesto stanovanja, ulica i broj za vreme studiranja:', 1),
(18, 'Mesto stanovanja, ulica i broj za vreme studiranja:', 2),
(19, 'Broj telefona:', 1),
(20, 'Broj telefona:', 2),
(21, 'Drzavljanstvo:', 1),
(22, 'Nacionalna pripadnost:', 1),
(23, 'Prethodna skolska sprema:', 1),
(24, 'Godina koju upisujete:', 1),
(25, 'Godina koju upisujete:', 2),
(26, 'Da li ponovo upisujete ovu godinu studija:', 1),
(27, 'Da li ponovo upisujete ovu godinu studija:', 2),
(28, 'Koje skolske godine ste se prvi put upisali na ovu vrstu studija?', 1),
(29, 'Zanimanje:', 1),
(30, 'Zanimanje:', 2),
(31, 'Svojstvo roditelj-student:', 1),
(32, 'Svojstvo roditelj-student:', 2),
(33, 'Stepen skolske spreme:', 1),
(34, 'Stepen skolske spreme:', 2),
(35, 'Poteskoce:', 1),
(36, 'Poteskoce:', 2);

-- --------------------------------------------------------

--
-- Table structure for table `predmet`
--

CREATE TABLE `predmet` (
  `id_predmeta` int(11) NOT NULL,
  `naziv_predmeta` varchar(60) NOT NULL,
  `espb` int(2) NOT NULL,
  `obavezni` varchar(1) NOT NULL,
  `smer` varchar(60) NOT NULL,
  `godina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `predmet`
--

INSERT INTO `predmet` (`id_predmeta`, `naziv_predmeta`, `espb`, `obavezni`, `smer`, `godina`) VALUES
(1, 'Uvod u računarstvo', 6, 'O', 'Računarstvo i informatika', 1),
(2, 'Algoritmi i programiranje', 6, 'O', 'Računarstvo i informatika', 1),
(3, 'Diskretna matematika', 6, 'O', 'Računarstvo i informatika', 2),
(4, 'Računarski sistemi', 6, 'O', 'Računarstvo i informatika', 2),
(5, 'Objektno-orjentisano programiranje', 6, 'O', 'Računarstvo i informatika', 2),
(6, 'Logičko projektovanje', 6, 'O', 'Računarstvo i informatika', 2),
(7, 'Arhitektura i organizacija računara', 6, 'O', 'Računarstvo i informatika', 2),
(8, 'Programski jezici', 6, 'O', 'Računarstvo i informatika', 2),
(9, 'Strukture podataka', 6, 'O', 'Računarstvo i informatika', 2),
(10, 'Baze podataka', 6, 'O', 'Računarstvo i informatika', 2),
(11, 'Teorija grafova', 6, 'I', 'Računarstvo i informatika', 2),
(12, 'Razvoj web aplikacija', 6, 'I', 'Računarstvo i informatika', 3),
(13, 'Distribuirani sistemi', 6, 'O', 'Računarstvo i informatika', 3),
(14, 'Teorija igara', 6, 'I', 'Računarstvo i informatika', 3),
(15, 'Osnovi analize signala i sistema', 6, 'I', 'Računarstvo i informatika', 3),
(16, 'Sistemi baza podataka', 6, 'I', 'Računarstvo i informatika', 3),
(17, 'Objektno-orjentisano projektovanje', 6, 'O', 'Računarstvo i informatika', 3),
(18, 'Operativni sistemi', 6, 'O', 'Računarstvo i informatika', 3),
(19, 'Računarske mreže', 6, 'O', 'Računarstvo i informatika', 3),
(20, 'Web programiranje', 6, 'O', 'Računarstvo i informatika', 3),
(21, 'Mikroračunarski sistemi', 6, 'O', 'Računarstvo i informatika', 3),
(22, 'Informacioni sistemi', 6, 'O', 'Računarstvo i informatika', 3),
(23, 'Softversko inžinjerstvo', 6, 'O', 'Računarstvo i informatika', 3),
(24, 'Interakcija čovek-računar', 6, 'O', 'Računarstvo i informatika', 3),
(25, 'Engleski jezik 1', 6, 'O', 'Računarstvo i informatika', 3),
(26, 'Engleski jezik 2', 6, 'O', 'Računarstvo i informatika', 3),
(27, 'Elektronske komponente', 6, 'O', 'none', 1),
(28, 'Fizika', 6, 'O', 'none', 1),
(29, 'Matematika I', 6, 'O', 'none', 1),
(30, 'Matematika II', 6, 'O', 'none', 1),
(31, 'Osnovi elektrotehnike I', 6, 'O', 'none', 1),
(32, 'Osnovi elektrotehnike II', 6, 'O', 'none', 1),
(33, 'Laboratorijski praktikum-Fizika', 3, 'O', 'none', 1),
(34, 'Laboratorijski praktikum-Elektronske komponente', 3, 'O', 'none', 1),
(35, 'Laboratorijski praktikum-Osnovi elektrotehnike', 3, 'O', 'none', 1),
(36, 'Laboratorijski praktikum-Elektronske komponente', 3, 'O', 'none', 1),
(37, 'Laboratorijski praktikum-Algoritmi i programiranje', 3, 'O', 'none', 1),
(38, 'Digitalna elektronika', 6, 'O', 'Upravljanje sistemima', 2),
(39, 'Električna kola', 6, 'O', 'Upravljanje sistemima', 2),
(40, 'Linearni sistemi automatskog upravljanja', 6, 'O', 'Upravljanje sistemima', 2),
(41, 'Matematika III', 6, 'O', 'Upravljanje sistemima', 2),
(42, 'Metrologija električnih veličina', 6, 'O', 'Upravljanje sistemima', 2),
(43, 'Mikrokontroleri i programiranje', 6, 'O', 'Upravljanje sistemima', 2),
(44, 'Modeliranje i simulacija dinamičkih sistema', 6, 'O', 'Upravljanje sistemima', 2),
(45, 'Osnovi elektronike', 6, 'O', 'Upravljanje sistemima', 2),
(46, 'Računarski sistemi', 6, 'O', 'Upravljanje sistemima', 2),
(47, 'Operaciona istraživanja', 6, 'O', 'Upravljanje sistemima', 2),
(48, 'Digitalna elektronika', 6, 'O', 'Telekomunikacije', 2),
(49, 'Električna kola i signali', 6, 'O', 'Telekomunikacije', 2),
(50, 'Elektromagnetika', 6, 'O', 'Telekomunikacije', 2),
(51, 'Matematika IV', 6, 'O', 'Telekomunikacije', 2),
(52, 'Matematika III', 6, 'O', 'Telekomunikacije', 2),
(53, 'Osnovi elektronike', 6, 'O', 'Telekomunikacije', 2),
(54, 'Osnovi mikrotalasne tehnike', 6, 'O', 'Telekomunikacije', 2),
(55, 'Osnovi telekomunikacija', 6, 'O', 'Telekomunikacije', 2),
(56, 'Teorija telekomunikacija', 6, 'O', 'Telekomunikacije', 2),
(57, 'Analogna elektronika', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(58, 'Digitalna elektronika elektronika', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(59, 'Digitalna obrada signala', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(60, 'Elektricna i elektronska merenja', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(61, 'Matematika III', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(62, 'Osnovi elektronike', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(63, 'Signali i sistemi', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(64, 'Telekomunikacije', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 2),
(65, 'Matematika III', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(66, 'Materijali za elektroniku', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(67, 'Metrologija električnih veličina', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(68, 'Osnovi elektronike', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(69, 'Osnovi optike', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(70, 'Poluprovodničke komponente', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(71, 'Signali i sistemi', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(72, 'Telekomunikacije', 6, 'O', 'Elektronske komponente i mikrosistemi', 2),
(73, 'Električna kola', 6, 'O', 'Elektroenergetika', 2),
(74, 'Električne instalacije', 6, 'O', 'Elektroenergetika', 2),
(75, 'Matematika III', 6, 'O', 'Elektroenergetika', 2),
(76, 'Elektrotehnički materijali', 6, 'O', 'Elektroenergetika', 2),
(77, 'Merenja u elektroenergetici', 6, 'O', 'Elektroenergetika', 2),
(78, 'Metrologija električnih veličina', 6, 'O', 'Elektroenergetika', 2),
(79, 'Osnovi elektronike', 6, 'O', 'Elektroenergetika', 2),
(80, 'Prenos električne energije', 6, 'O', 'Elektroenergetika', 2),
(81, 'Tehnička mehanika', 6, 'O', 'Elektroenergetika', 2),
(82, 'Transformatori i mašine jednosmerne struje', 6, 'O', 'Elektroenergetika', 2),
(83, 'Baze podataka', 6, 'O', 'Upravljanje sistemima', 3),
(84, 'Bežični komunikacioni sistemi', 6, 'O', 'Upravljanje sistemima', 3),
(85, 'Digitalna obrada signala', 6, 'O', 'Upravljanje sistemima', 3),
(86, 'Elektronska merenja', 6, 'O', 'Upravljanje sistemima', 3),
(87, 'Informacioni sistemi', 6, 'O', 'Upravljanje sistemima', 3),
(88, 'Mehatronika', 6, 'O', 'Upravljanje sistemima', 3),
(89, 'Mikroračunarski sistemi', 6, 'O', 'Upravljanje sistemima', 3),
(90, 'Nelinearni SAU', 6, 'O', 'Upravljanje sistemima', 3),
(91, 'Optimalno upravljanje', 6, 'O', 'Upravljanje sistemima', 3),
(92, 'Senzori i pretvarači', 6, 'O', 'Upravljanje sistemima', 3),
(93, 'Solarne komponente i sistemi', 6, 'O', 'Upravljanje sistemima', 3),
(94, 'Telekomunikacije', 6, 'O', 'Upravljanje sistemima', 3),
(95, 'Upravljanje procesima', 6, 'O', 'Upravljanje sistemima', 3),
(258, 'Digitalne telekomunikacije I', 6, 'O', 'Telekomunikacije', 3),
(259, 'Elektroakustika', 6, 'O', 'Telekomunikacije', 3),
(260, 'Mikrotalasna tehnika', 6, 'O', 'Telekomunikacije', 3),
(261, 'Telekomunikacione mreže', 6, 'O', 'Telekomunikacije', 3),
(262, 'Teorija informacija', 6, 'I', 'Telekomunikacije', 3),
(263, 'Engleski jezik 1', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 3),
(264, 'Engleski jezik 1', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 3),
(265, 'Engleski jezik 2', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 3),
(266, 'Engleski jezik 2', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 3),
(267, 'Analogna mikroelektronika', 6, 'O', 'Elektronske komponente i mikrosistemi', 3),
(268, 'Engleski jezik 1', 6, 'O', 'Elektronske komponente i mikrosistemi', 3),
(269, 'Engleski jezik 2', 6, 'O', 'Elektronske komponente i mikrosistemi', 3),
(270, 'Novi materijali i tehnologije', 6, 'I', 'Elektronske komponente i mikrosistemi', 3),
(271, 'Optoelektronika', 6, 'O', 'Elektronske komponente i mikrosistemi', 3),
(272, 'Projektovanje štampanih ploča', 6, 'I', 'Elektronske komponente i mikrosistemi', 3),
(273, 'Automatsko upravljanje', 6, 'O', 'Elektroenergetika', 3),
(274, 'Elektromagnetika', 6, 'O', 'Elektroenergetika', 3),
(275, 'Elektronska merenja', 6, 'I', 'Elektroenergetika', 3),
(276, 'Energetska elektronika', 6, 'O', 'Elektroenergetika', 3),
(277, 'Upravljanje procesima', 6, 'I', 'Elektroenergetika', 3),
(278, 'Paralelni sistemi', 6, 'O', 'Računarstvo i informatika', 4),
(279, 'Računarska grafika', 6, 'O', 'Računarstvo i informatika', 4),
(280, 'Programski prevodioci', 6, 'O', 'Računarstvo i informatika', 4),
(281, 'Napredne baze podataka', 6, 'I', 'Računarstvo i informatika', 4),
(282, 'Projektovanje računarskih mreža', 6, 'I', 'Računarstvo i informatika', 4),
(283, 'Zaštita informacija', 6, 'I', 'Računarstvo i informatika', 4),
(284, 'Medicinska informatika', 6, 'I', 'Računarstvo i informatika', 5),
(285, 'Testiranje i kvalitet softvera', 6, 'I', 'Računarstvo i informatika', 5),
(286, 'Upravljanje projektima', 6, 'I', 'Računarstvo i informatika', 5),
(287, 'Kompleksnost algoritama', 6, 'I', 'Računarstvo i informatika', 5),
(288, 'Elektronika u medicini', 6, 'I', 'Upravljanje sistemima', 4),
(289, 'Elektromotorni pogoni', 6, 'O', 'Upravljanje sistemima', 4),
(290, 'Elektroenergetski pretvarači', 6, 'O', 'Upravljanje sistemima', 4),
(291, 'Inžinjerska etika', 6, 'I', 'Upravljanje sistemima', 4),
(292, 'Identifikacija sistema', 6, 'O', 'Upravljanje sistemima', 4),
(293, 'Fazi skupovi i logika', 6, 'I', 'Upravljanje sistemima', 5),
(294, 'Inteligentni sistemi', 6, 'O', 'Upravljanje sistemima', 5),
(295, 'Inteligentne mašine', 6, 'I', 'Upravljanje sistemima', 5),
(296, 'Adaptivna obrada signala', 6, 'O', 'Upravljanje sistemima', 5),
(297, 'Menadžment kvaliteta', 6, 'I', 'Upravljanje sistemima', 5),
(298, 'Numerička matematika', 6, 'I', 'Upravljanje sistemima', 5),
(299, 'Kodovanje', 6, 'O', 'Telekomunikacije', 4),
(300, 'Komunikaciona akustika', 6, 'O', 'Telekomunikacije', 4),
(301, 'Optičke mreže', 6, 'O', 'Telekomunikacije', 4),
(302, 'Antene i prostiranje', 6, 'I', 'Telekomunikacije', 4),
(303, 'Mobilni komunikacioni sistemi', 6, 'I', 'Telekomunikacije', 4),
(304, 'Digitalna obrada slike', 6, 'I', 'Telekomunikacije', 5),
(305, 'Nelinearna optika', 6, 'O', 'Telekomunikacije', 5),
(306, 'Bežični pristup internetu', 6, 'I', 'Telekomunikacije', 5),
(307, 'Paketske komunikacije', 6, 'I', 'Telekomunikacije', 5),
(308, 'Izvori za napajanje', 6, 'O', 'Elektroenergetika', 4),
(309, 'Elektrane', 6, 'O', 'Elektroenergetika', 4),
(310, 'Elektromotorni pogoni', 6, 'O', 'Elektroenergetika', 4),
(311, 'Zaštita u elektroenergetici', 6, 'I', 'Elektroenergetika', 4),
(312, 'Telekomunikacije u energetici', 6, 'I', 'Elektroenergetika', 5),
(313, 'Regulacija elektromotornih pogona', 6, 'I', 'Elektroenergetika', 5),
(314, 'Računarsko merno-informacioni sistemi', 6, 'I', 'Elektroenergetika', 5),
(315, 'Eksploatacija elektroenergetskih mreža', 6, 'I', 'Elektroenergetika', 5),
(316, 'Digitalna obrada slike', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 4),
(317, 'Izvori za napajanje', 6, 'I', 'Elektronika i mikroprocesorska tehnika', 4),
(318, 'Mikrokontroleri', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 4),
(319, 'Obnovljivi izvori energije', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 4),
(320, 'RF sistemi', 6, 'I', 'Elektronika i mikroprocesorska tehnika', 5),
(321, 'Mikroelektromehanički sistemi', 6, 'O', 'Elektronika i mikroprocesorska tehnika', 5),
(322, 'Širokopojasne mreže za pristup', 6, 'I', 'Elektronika i mikroprocesorska tehnika', 5),
(323, 'Izvori za napajanje', 6, 'O', 'Elektronske komponente i mikrosistemi', 4),
(324, 'Komponente i kola snage', 6, 'O', 'Elektronske komponente i mikrosistemi', 4),
(325, 'Obnovljivi izvori energije', 6, 'O', 'Elektronske komponente i mikrosistemi', 4),
(326, 'Projektovanje mikrosistema', 6, 'O', 'Elektronske komponente i mikrosistemi', 4),
(327, 'Solarne komponente i sistemi', 6, 'O', 'Elektronske komponente i mikrosistemi', 4);

-- --------------------------------------------------------

--
-- Table structure for table `pripada`
--

CREATE TABLE `pripada` (
  `id` int(11) NOT NULL,
  `id_predmeta` int(11) NOT NULL,
  `id_semestra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profesor`
--

CREATE TABLE `profesor` (
  `id_profesora` int(11) NOT NULL,
  `ime_profesora` varchar(32) NOT NULL,
  `zvanje` varchar(15) NOT NULL,
  `korisnicko_ime` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `semestar`
--

CREATE TABLE `semestar` (
  `id_semestra` int(11) NOT NULL,
  `broj_semestra` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `semestar`
--

INSERT INTO `semestar` (`id_semestra`, `broj_semestra`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8);

-- --------------------------------------------------------

--
-- Table structure for table `slika`
--

CREATE TABLE `slika` (
  `id_slike` int(11) NOT NULL,
  `indeks` int(6) DEFAULT NULL,
  `id_profesora` int(11) DEFAULT NULL,
  `putanja` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sluša`
--

CREATE TABLE `sluša` (
  `indeks` int(6) DEFAULT NULL,
  `id_predmeta` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `indeks` int(6) NOT NULL,
  `modul` varchar(32) NOT NULL,
  `smer` varchar(32) NOT NULL,
  `prezime` varchar(32) NOT NULL,
  `ime_roditelja` varchar(32) NOT NULL,
  `ime` varchar(32) NOT NULL,
  `jmbg` varchar(13) NOT NULL,
  `pol` varchar(1) NOT NULL,
  `godina_rodjenja` date NOT NULL,
  `mesto_rodjenja` varchar(50) NOT NULL,
  `adresa` varchar(50) NOT NULL,
  `grad` varchar(32) NOT NULL,
  `drzava` varchar(32) NOT NULL,
  `mesto_stanovanja` varchar(50) NOT NULL,
  `broj_telefona` varchar(10) NOT NULL,
  `drzavljanstvo` varchar(32) NOT NULL,
  `nacionalna_pripadnost` varchar(32) NOT NULL,
  `skolska_sprema` varchar(32) NOT NULL,
  `stepen_obrazovanja_oca` varchar(32) NOT NULL,
  `stepen_obrazovanja_majke` varchar(32) NOT NULL,
  `poteskoce` varchar(100) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `password` (`password`),
  ADD UNIQUE KEY `korisnicko_ime` (`korisnicko_ime`);

--
-- Indexes for table `anketa`
--
ALTER TABLE `anketa`
  ADD PRIMARY KEY (`id_ankete`);

--
-- Indexes for table `drzi`
--
ALTER TABLE `drzi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drzi_ibfk_1` (`id_predmeta`),
  ADD KEY `drzi` (`id_profesora`);

--
-- Indexes for table `odgovori`
--
ALTER TABLE `odgovori`
  ADD PRIMARY KEY (`id_odgovora`),
  ADD KEY `id_pitanja` (`id_pitanja`),
  ADD KEY `id_studenta` (`id_studenta`);

--
-- Indexes for table `pitanja`
--
ALTER TABLE `pitanja`
  ADD PRIMARY KEY (`id_pitanja`),
  ADD KEY `id_ankete` (`id_ankete`);

--
-- Indexes for table `predmet`
--
ALTER TABLE `predmet`
  ADD PRIMARY KEY (`id_predmeta`);

--
-- Indexes for table `pripada`
--
ALTER TABLE `pripada`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pripada` (`id_predmeta`),
  ADD KEY `id_semestra` (`id_semestra`);

--
-- Indexes for table `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`id_profesora`),
  ADD UNIQUE KEY `korisnicko_ime` (`korisnicko_ime`),
  ADD UNIQUE KEY `password` (`password`);

--
-- Indexes for table `semestar`
--
ALTER TABLE `semestar`
  ADD PRIMARY KEY (`id_semestra`);

--
-- Indexes for table `slika`
--
ALTER TABLE `slika`
  ADD PRIMARY KEY (`id_slike`),
  ADD KEY `indeks` (`indeks`),
  ADD KEY `id_profesora` (`id_profesora`);

--
-- Indexes for table `sluša`
--
ALTER TABLE `sluša`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indeks` (`indeks`),
  ADD KEY `id_predmeta` (`id_predmeta`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`indeks`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `anketa`
--
ALTER TABLE `anketa`
  MODIFY `id_ankete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `drzi`
--
ALTER TABLE `drzi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `odgovori`
--
ALTER TABLE `odgovori`
  MODIFY `id_odgovora` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pitanja`
--
ALTER TABLE `pitanja`
  MODIFY `id_pitanja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `predmet`
--
ALTER TABLE `predmet`
  MODIFY `id_predmeta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=328;
--
-- AUTO_INCREMENT for table `pripada`
--
ALTER TABLE `pripada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profesor`
--
ALTER TABLE `profesor`
  MODIFY `id_profesora` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `semestar`
--
ALTER TABLE `semestar`
  MODIFY `id_semestra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `drzi`
--
ALTER TABLE `drzi`
  ADD CONSTRAINT `drzi_ibfk_1` FOREIGN KEY (`id_predmeta`) REFERENCES `predmet` (`id_predmeta`),
  ADD CONSTRAINT `drzi_ibfk_2` FOREIGN KEY (`id_profesora`) REFERENCES `profesor` (`id_profesora`);

--
-- Constraints for table `odgovori`
--
ALTER TABLE `odgovori`
  ADD CONSTRAINT `odgovori_ibfk_1` FOREIGN KEY (`id_pitanja`) REFERENCES `pitanja` (`id_pitanja`),
  ADD CONSTRAINT `odgovori_ibfk_2` FOREIGN KEY (`id_studenta`) REFERENCES `student` (`indeks`);

--
-- Constraints for table `pitanja`
--
ALTER TABLE `pitanja`
  ADD CONSTRAINT `pitanja_ibfk_1` FOREIGN KEY (`id_ankete`) REFERENCES `anketa` (`id_ankete`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pripada`
--
ALTER TABLE `pripada`
  ADD CONSTRAINT `pripada_ibfk_1` FOREIGN KEY (`id_predmeta`) REFERENCES `predmet` (`id_predmeta`),
  ADD CONSTRAINT `pripada_ibfk_2` FOREIGN KEY (`id_semestra`) REFERENCES `semestar` (`id_semestra`);

--
-- Constraints for table `slika`
--
ALTER TABLE `slika`
  ADD CONSTRAINT `slika_ibfk_1` FOREIGN KEY (`indeks`) REFERENCES `student` (`indeks`),
  ADD CONSTRAINT `slika_ibfk_2` FOREIGN KEY (`id_profesora`) REFERENCES `profesor` (`id_profesora`);

--
-- Constraints for table `sluša`
--
ALTER TABLE `sluša`
  ADD CONSTRAINT `sluša_ibfk_1` FOREIGN KEY (`indeks`) REFERENCES `student` (`indeks`),
  ADD CONSTRAINT `sluša_ibfk_2` FOREIGN KEY (`id_predmeta`) REFERENCES `predmet` (`id_predmeta`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
