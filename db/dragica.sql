-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2017 at 07:02 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dragica`
--

-- --------------------------------------------------------

--
-- Table structure for table `anketa`
--

CREATE TABLE `anketa` (
  `id_ankete` int(11) NOT NULL,
  `naziv` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `anketa`
--

INSERT INTO `anketa` (`id_ankete`, `naziv`) VALUES
(1, 'Upis na fakultet'),
(2, 'Izmene');

-- --------------------------------------------------------

--
-- Table structure for table `drzi`
--

CREATE TABLE `drzi` (
  `id` int(11) NOT NULL,
  `id_predmeta` int(11) DEFAULT NULL,
  `id_profesora` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `odgovori`
--

CREATE TABLE `odgovori` (
  `id_odgovora` int(11) NOT NULL,
  `id_pitanja` int(11) NOT NULL,
  `id_studenta` int(11) NOT NULL,
  `odgovor` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pitanja`
--

CREATE TABLE `pitanja` (
  `id_pitanja` int(11) NOT NULL,
  `pitanje` varchar(300) NOT NULL,
  `id_ankete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pitanja`
--

INSERT INTO `pitanja` (`id_pitanja`, `pitanje`, `id_ankete`) VALUES
(1, 'Dosije broj:', 1),
(2, 'Semestar po redu:', 1),
(5, 'Semestar po redu:', 2),
(6, 'Semestar:', 1),
(7, 'Semestar:', 2),
(8, 'Skolska godina:', 1),
(9, 'Skolska godina:', 2),
(10, 'Finansiranje:', 1),
(11, 'Finansiranje:', 2),
(12, 'Prezime, ime jednog od roditelja, ime studenta:', 1),
(13, 'JMBG:', 1),
(14, 'Pol:', 1),
(15, 'Datum rodjenja:', 1),
(16, 'Mesto rodjenja, opstina, republika:', 1),
(17, 'Mesto stanovanja, ulica i broj za vreme studiranja:', 1),
(18, 'Mesto stanovanja, ulica i broj za vreme studiranja:', 2),
(19, 'Broj telefona:', 1),
(20, 'Broj telefona:', 2),
(21, 'Drzavljanstvo:', 1),
(22, 'Nacionalna pripadnost:', 1),
(23, 'Prethodna skolska sprema:', 1),
(24, 'Godina koju upisujete:', 1),
(25, 'Godina koju upisujete:', 2),
(26, 'Da li ponovo upisujete ovu godinu studija:', 1),
(27, 'Da li ponovo upisujete ovu godinu studija:', 2),
(28, 'Koje skolske godine ste se prvi put upisali na ovu vrstu studija?', 1),
(29, 'Zanimanje:', 1),
(30, 'Zanimanje:', 2),
(31, 'Svojstvo roditelj-student:', 1),
(32, 'Svojstvo roditelj-student:', 2),
(33, 'Stepen skolske spreme:', 1),
(34, 'Stepen skolske spreme:', 2),
(35, 'Poteskoce:', 1),
(36, 'Poteskoce:', 2);

-- --------------------------------------------------------

--
-- Table structure for table `predmet`
--

CREATE TABLE `predmet` (
  `id_predmeta` int(11) NOT NULL,
  `naziv_predmeta` varchar(10) NOT NULL,
  `espb` int(2) NOT NULL,
  `obavezni` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pripada`
--

CREATE TABLE `pripada` (
  `id` int(11) NOT NULL,
  `id_predmeta` int(11) NOT NULL,
  `id_semestra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profesor`
--

CREATE TABLE `profesor` (
  `id_profesora` int(11) NOT NULL,
  `ime_profesora` varchar(32) NOT NULL,
  `zvanje` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `semestar`
--

CREATE TABLE `semestar` (
  `id_semestra` int(11) NOT NULL,
  `broj_semestra` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `indeks` int(6) NOT NULL,
  `modul` varchar(32) NOT NULL,
  `smer` varchar(32) NOT NULL,
  `prezime` varchar(32) NOT NULL,
  `ime_roditelja` varchar(32) NOT NULL,
  `ime` varchar(32) NOT NULL,
  `jmbg` varchar(13) NOT NULL,
  `pol` varchar(1) NOT NULL,
  `godina_rodjenja` date NOT NULL,
  `mesto_rodjenja` varchar(50) NOT NULL,
  `adresa` varchar(50) NOT NULL,
  `grad` varchar(32) NOT NULL,
  `drzava` varchar(32) NOT NULL,
  `mesto_stanovanja` varchar(50) NOT NULL,
  `broj_telefona` varchar(10) NOT NULL,
  `drzavljanstvo` varchar(32) NOT NULL,
  `nacionalna_pripadnost` varchar(32) NOT NULL,
  `skolska_sprema` varchar(32) NOT NULL,
  `stepen_obrazovanja_oca` varchar(32) NOT NULL,
  `stepen_obrazovanja_majke` varchar(32) NOT NULL,
  `poteskoce` varchar(100) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anketa`
--
ALTER TABLE `anketa`
  ADD PRIMARY KEY (`id_ankete`);

--
-- Indexes for table `drzi`
--
ALTER TABLE `drzi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drzi_ibfk_1` (`id_predmeta`),
  ADD KEY `drzi` (`id_profesora`);

--
-- Indexes for table `odgovori`
--
ALTER TABLE `odgovori`
  ADD PRIMARY KEY (`id_odgovora`),
  ADD KEY `id_pitanja` (`id_pitanja`),
  ADD KEY `id_studenta` (`id_studenta`);

--
-- Indexes for table `pitanja`
--
ALTER TABLE `pitanja`
  ADD PRIMARY KEY (`id_pitanja`),
  ADD KEY `id_ankete` (`id_ankete`);

--
-- Indexes for table `predmet`
--
ALTER TABLE `predmet`
  ADD PRIMARY KEY (`id_predmeta`);

--
-- Indexes for table `pripada`
--
ALTER TABLE `pripada`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pripada` (`id_predmeta`),
  ADD KEY `id_semestra` (`id_semestra`);

--
-- Indexes for table `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`id_profesora`);

--
-- Indexes for table `semestar`
--
ALTER TABLE `semestar`
  ADD PRIMARY KEY (`id_semestra`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`indeks`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anketa`
--
ALTER TABLE `anketa`
  MODIFY `id_ankete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `drzi`
--
ALTER TABLE `drzi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `odgovori`
--
ALTER TABLE `odgovori`
  MODIFY `id_odgovora` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pitanja`
--
ALTER TABLE `pitanja`
  MODIFY `id_pitanja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `predmet`
--
ALTER TABLE `predmet`
  MODIFY `id_predmeta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pripada`
--
ALTER TABLE `pripada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profesor`
--
ALTER TABLE `profesor`
  MODIFY `id_profesora` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `semestar`
--
ALTER TABLE `semestar`
  MODIFY `id_semestra` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `drzi`
--
ALTER TABLE `drzi`
  ADD CONSTRAINT `drzi_ibfk_1` FOREIGN KEY (`id_predmeta`) REFERENCES `predmet` (`id_predmeta`),
  ADD CONSTRAINT `drzi_ibfk_2` FOREIGN KEY (`id_profesora`) REFERENCES `profesor` (`id_profesora`);

--
-- Constraints for table `odgovori`
--
ALTER TABLE `odgovori`
  ADD CONSTRAINT `odgovori_ibfk_1` FOREIGN KEY (`id_pitanja`) REFERENCES `pitanja` (`id_pitanja`),
  ADD CONSTRAINT `odgovori_ibfk_2` FOREIGN KEY (`id_studenta`) REFERENCES `student` (`indeks`);

--
-- Constraints for table `pitanja`
--
ALTER TABLE `pitanja`
  ADD CONSTRAINT `pitanja_ibfk_1` FOREIGN KEY (`id_ankete`) REFERENCES `anketa` (`id_ankete`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pripada`
--
ALTER TABLE `pripada`
  ADD CONSTRAINT `pripada_ibfk_1` FOREIGN KEY (`id_predmeta`) REFERENCES `predmet` (`id_predmeta`),
  ADD CONSTRAINT `pripada_ibfk_2` FOREIGN KEY (`id_semestra`) REFERENCES `semestar` (`id_semestra`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
