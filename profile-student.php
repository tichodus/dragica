<?php
    
    require_once 'style.html';
    require_once 'classes.php';
    require_once 'functions.php';
    session_start();
    $uploadOk = -1;
    $korisnik = $_SESSION['korisnik'];   
    $path=getImage($_SESSION['korisnik']);
    $prosek=vratiProsek($korisnik->index);
        
    if(isset($_POST["success"])) {
        
    if($_FILES['image']['size']>0){
    $target_dir = "Profilne/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image  
    // Check if file already exists
    // Check file size
    if ($_FILES["image"]["size"] > 1000000) {
       // echo "<div class='alert alert-warning' ><label>Fajl je previse veliki</label></div>";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
       // echo "<div class='alert alert-warning' ><label>Podrzani su samo JPG, JPEG, i PNG  formati.</label></div>";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
       // echo "<div class='alert alert-danger' ><label>Fajl nije uploadovan.</label></div>";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
           // echo "<div class='alert alert-success' ><label>Uspesno ste uploadovali sliku.</label></div>";
            insertPicture($target_file,$_SESSION['korisnik']);
            $path = getImage($_SESSION['korisnik']);
            
        } else {
            //echo "<div class='alert alert-danger' ><label>Fajl nije uploadovan.</label></div>";
        }
    }
    }
    else
        $uploadOk = 2;
    }
   // echo $path;
?>

<body style="background-image:url('Images/mybg.png');" class=''>
<div id='parent'>
    <div id='container' >
    

    <?php 
        if($uploadOk == 0){
           echo "<div class='alert alert-danger' ><label>Fajl nije uploadovan.</label></div>";
        }
        if($uploadOk == 1)
        {
            echo "<div class='alert alert-success' ><label>Uspesno ste uploadovali sliku.</label></div>";
        }
        if($uploadOk == 2)
        {
            echo "<div class='alert alert-danger' ><label>Morate izabrati sliku klikom na link Promeni sliku</label></div>";
        }
    ?>
    <div>
        <div id='info' class='col-xs-12 col-sm-12 col-md-12' >
            <div id='profilna'  class='col-xs-12 col-sm-12 col-md-12' style='height:inherit;'>
                <div class=''>
                    <img src='<?php echo $path; ?>' class='img-thumbnail img-rounded' style=' height:200px; width:200px;'>
                </div>
                <div class='col-xs-12 col-sm-12 col-md-12' >
                    <form action='' method='post' enctype="multipart/form-data">
                        <div class='col-xs-12 col-sm-6 col-md-6' >
                            <label class="btn btn-default btn-link">
                                Promeni sliku<input type="file" name='image' style="display: none;">
                            </label>
                        </div>
                        <div class='col-xs-12 col-sm-12 col-md-12' >
                             <input type='submit' class='btn btn-link btn-file' name='success' value='Snimi' />
                        </div>
                    </form>
                </div>

                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-12 col-md-12' >
                   <label style='margin-top:2em;'><b>Indeks:</b> <?php echo ucfirst($korisnik -> index); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Ime: <?php echo ucfirst($korisnik -> ime); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Prezime: <?php echo ucfirst($korisnik -> prezime); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Modul: <?php echo ucfirst($korisnik -> modul); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Smer: <?php echo ucfirst($korisnik -> smer); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>JMBG: <?php echo ucfirst($korisnik -> jmbg); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Datum rodjenja: <?php echo ucfirst($korisnik -> god_rodjenja); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Mesto rodjenja: <?php echo ucfirst($korisnik -> mesto_rodjenja); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Adresa: <?php echo ucfirst($korisnik -> adresa); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Grad: <?php echo ucfirst($korisnik -> grad); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Drzava: <?php echo ucfirst($korisnik -> drzava); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Mesto stanovanja: <?php echo ucfirst($korisnik -> mesto_stanovanja); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;' >Telefon: <?php echo ucfirst($korisnik -> telefon); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Drzavljanstvo: <?php echo ucfirst($korisnik -> drzavljanstvo); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Nacionalnost: <?php echo ucfirst($korisnik -> nacionalnost); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Srednja skola: <?php echo ucfirst($korisnik -> skolovanje); ?></label>
                </div>
                <div style='border-bottom:1px solid black;' class='col-xs-12 col-sm-6 col-md-6' >
                   <label style='margin-top:1em;'>Prosecna ocena: <?php echo $prosek; ?></label>
                </div>
                <div class='col-xs-12 col-sm-7 col-md-7' >
                    <input onclick="izmeni()" style='margin-top:2em;margin-bottom:2em;' class='btn btn-success btn-lg' type='submit' name='prosledi' value='Izmeni podatke'/>
                </div>
                <div  class='col-xs-12 col-sm-5 col-md-5' >
                    <input onclick="lozinka()" style='margin-top:2em;margin-bottom:2em;' class='btn btn-success btn-lg' type='submit' name='promena lozinke' value='Izmeni lozinku'/>
                </div>
            </div>
            
        </div>

      
        </div>
    </div>
    </div>
<script>
    function izmeni() {
        window.location.href="izmena.php";
    }
    function lozinka(){
        window.location.href="promena_lozinke.php";
    }
</script>
 <script src='src/logout.js'></script>
 <script src='src/animation.js'></script>
 
</body>