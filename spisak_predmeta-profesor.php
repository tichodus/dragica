<?php
require_once 'style.html';
include_once 'functions.php';
session_start();
$korisnik=$_SESSION['korisnik'];


$predmeti=vrati_predmete_koje_drzi_profesor($korisnik->id);
foreach($predmeti as $predmet){
    if(isset($_GET['id'])){
        $id=$_GET['id'];
        if($id==$predmet->id){
        $_SESSION['predmet']=$predmet;
        header('Location: predmet-profesor.php');
        }

    }
}



?>
<body id='parent'style='background-image:url("Images/mybg.png");'>
    <div id='container' class='container'>
    <h1 class='jumbotron'>Lista predmeta koje drzi profesor:</h1>
    <div class='list-group'>
    <?php
    
    foreach($predmeti as $predmet){
        echo "<a class='list-group-item' href='spisak_predmeta-profesor.php?id=$predmet->id'>$predmet->naziv</a>";
    }
    ?>
    </div>
    </div>
    <script src='src/animation.js'></script>
</body>
