<?php
require_once 'style.html';
?>

<body style="background-image:url('Images/mybg.png');" class=''>
    <div id='parent' class='col-xs-12 col-sm-12 col-md-12'>
        <div class='col-md-10'>
        <div style='position:relative;' class='container'><h2 class='jumbotron'>Dobrodosli na DRAGICA portal</h2></div>
        <div align='left' class='container' style='height:700px;'>         
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div id='carousel-inner' class="carousel-inner">
            <div class="item active">
                <img src="slike/slika1.jpg" id='img-active' class='img-responsive' alt="Los Angeles">
            </div>

        
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
            </a>
            </div>
        </div>
        </div>
        <div class='col-md-2' id='rss' style='height:100%;' >
             <?php require_once 'rss.php' ?>
        </div>
</div>

<script src='src/get_images.js'></script>
<script src='src/animation.js'></script>
</body>
