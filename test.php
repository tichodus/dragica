<?php
require_once 'style.html';
include_once 'functions.php';
session_start();
$korisnik=$_SESSION['korisnik'];
$prosek='';

    $ocene=sveOcene($korisnik->index);
    $iscrtavanje=false;
    $iscrtavanje2=true;
    if(isset($_POST['izaberi'])){
       $iscrtavanje=true;
       $iscrtavanje2=false;
       $ocene=vratiOcene($korisnik->index,$_POST['semestar']);
    }
        $prosek=vratiProsek($korisnik->index);
        $_SESSION['prosek'] = $prosek;
?>

<body style='background-image:url("Images/mybg.png");'>
    <div class='container'>
        <h1 class='jumbotron'>Prikaz ocena iz odabranog semestra</h1>
        <form action='' method='post'>
            <div class='col-xs-12 col-sm-6 col-md-6'>
                <label for='semestar' >Izaberite semestar:</label>
                <select class='form-control'  name='semestar'>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                </select>
            </div>
            <div class='col-xs-12 col-sm-6 col-md-6'>
                </br><input type='submit' class='btn btn-default btn-lg' name='izaberi' value='Izaberi' />
            </div>
        </form>
    </div>

    <div class='container'>
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <?php
                if($iscrtavanje2){
                    
                    print("<h3>Prikaz ocena iz svih predmeta:</h3>\n");
                    print("<table class='table'>\n");
                    echo '<thead>';
                    print("<tr><th>Naziv predmeta</th><th>Ocena</th></tr>\n");
                    echo '</thead>';
                    echo '<tbody>';
                    foreach($ocene as $ocena){
                        $predmet=vratiPredmet($ocena->predmet);
                        print("<tr>\n");
                        print("<td>$predmet->naziv</td><td>$ocena->ocena</td>\n");
                        print("</tr>\n");
                    }
                    echo '</tbody>';
                    print("</table>\n");
                    
                }
                if($iscrtavanje){
                    
                    print("<h3>Prikaz ocena iz odabranog semestra:</h3>\n");
                    print("<table class='table'>\n");
                    echo '<thead>';
                    print("<tr><th>Naziv predmeta</th><th>Ocena</th></tr>\n");
                    echo '</thead>';
                    echo '<tbody>';
                    foreach($ocene as $ocena){
                        $predmet=vratiPredmet($ocena->predmet);
                        print("<tr>\n");
                        print("<td>$predmet->naziv</td><td>$ocena->ocena</td>\n");
                        print("</tr>\n");
                    }
                    echo '</tbody>';
                    print("</table>\n");
                    ;
                }
            ?>
        </div>
    </div>


<script src='src/animation.js'></script>
</body>