<?php

  class Student{
	public $index;
	public $modul;
	public $smer;
	public $prezime;
	public $ime_roditelja;
	public $ime;
	public $jmbg;
	public $pol;
	public $god_rodjenja;
	public $mesto_rodjenja;
	public $adresa;
	public $grad;
	public $drzava;
	public $mesto_stanovanja;
	public $telefon;
	public $drzavljanstvo;
	public $nacionalnost;
	public $skolovanje;
	public $obrazovanje_oca;
	public $obrazovanje_majke;
	public $poteskoce;
	public $lozinka;
	public $id_semestra;


	public function __construct($index,$modul,$smer,$prezime,$ime_roditelja,$ime,$jmbg,$pol,$god_rodjenja,$mesto_rodjenja,$adresa,$grad,$drzavljanstvo,$drzava,$mesto_stanovanja,$telefon,$nacionalnost,$skolovanje,$obrazovanje_majke,$obrazovanje_oca,$poteskoce,$lozinka,$id_semestra){
		$this->index=$index;
		$this->modul=$modul;
		$this->smer=$smer;
		$this->prezime=$prezime;
		$this->ime_roditelja=$ime_roditelja;
		$this->ime=$ime;
		$this->jmbg=$jmbg;
		$this->pol=$pol;
		$this->god_rodjenja=$god_rodjenja;
		$this->mesto_rodjenja=$mesto_rodjenja;
		$this->adresa=$adresa;
		$this->grad=$grad;
		$this->drzava=$drzava;
		$this->mesto_stanovanja=$mesto_stanovanja;
		$this->telefon=$telefon;
		$this->drzavljanstvo=$drzavljanstvo;
		$this->nacionalnost=$nacionalnost;
		$this->skolovanje=$skolovanje;
		$this->obrazovanje_oca=$obrazovanje_oca;
		$this->obrazovanje_majke=$obrazovanje_majke;
		$this->poteskoce=$poteskoce;
		$this->lozinka=$lozinka;
		$this->id_semestra= $id_semestra;
	}
}

class Admin{
	public $id;
	public $korisnicko_ime;

	public function __construct($id, $korisnicko_ime){
		$this -> id = $id;
		$this -> korisnicko_ime = $korisnicko_ime;
	}
}
class Profesor{
	public $id;
	public $ime;
	public $zvanje;
	public $korisnicko_ime;
	public function __construct($id,$ime,$zvanje,$korisnicko_ime){
		$this->id=$id;
		$this->ime=$ime;
		$this->zvanje=$zvanje;
		$this->korisnicko_ime = $korisnicko_ime;
	}
}
class PredmetPrikaz{
	public $id;
	public $naziv;
	public $espb;
	public $ime_profesora;
	public function __construct($id,$naziv,$espb,$ime_profesora){
		$this->id=$id;
		$this->naziv=$naziv;
		$this->espb=$espb;
		$this->ime_profesora = $ime_profesora;
	}
}
class Predmet{
	public $id;
	public $naziv;
	public $espb;
	public $obavezni;
	public $smer;
	public $godina;
	public $opis;

	public function __construct($id,$naziv,$espb,$obavezni,$smer,$godina,$opis){
		$this->id=$id;
		$this->naziv=$naziv;
		$this->espb=$espb;
		$this->obavezni=$obavezni;
		$this->smer=$smer;
		$this->godina=$godina;
		$this->opis = $opis;
	}

}
class Semestar{
	public $id;
	public $broj;

	public function __construct($id,$broj){
		$this->id=$id;
		$this->broj=$broj;
	}
}
class Anketa{
		public $id_studenta;
        public $id_odgovora;
        public $id_ankete;
        
        public function __construct($br, $br1, $br2){
            $this->id_studenta = $br;
            $this->id_odgovora = $br1;
            $this->id_ankete = $br2;
	}
}
class Odgovori{
	public $id_predmeta;
	public $id_profesora;
	public $question1;
        public $question2;
        public $question3;
        public $question4;
        public $question5;
        public $question6;
        public $question7;
        public $question8;
        public $question9;
        public $question10;
        public $question11;
        public $question12;
        public $question13;
        public $question14;
        public $question15;
        public $question16;
        public $question17;
        public $question18;
        public $question19;
        public $question20;
        public $question21;
        public $question22;
        

	public function __construct($objekat, $predid, $profid){
            $this->id_predmeta = $predid;
            $this->id_profesora = $profid;
            $this->question1 = $objekat['question1'];
            $this->question2 = $objekat['question2'];
            $this->question3 = $objekat['question3'];
            $this->question4 = $objekat['question4'];
            $this->question5 = $objekat['question5'];
            $this->question6 = $objekat['question6'];
            $this->question7 = $objekat['question7'];
            $this->question8 = $objekat['question8'];
            $this->question9 = $objekat['question9'];
            $this->question10 = $objekat['question10'];
            $this->question11 = $objekat['question11'];
            $this->question12 = $objekat['question12'];
            if($objekat['question13']){
                $this->question13 = $objekat['question13'];
            }
            else {
                $this->question13 = "";
            }
            if($objekat['question14']){
                $this->question14 = $objekat['question14'];
            }
            else {
                $this->question14 = "";
            }
            $this->question15 = $objekat['question15'];
            $this->question16 = $objekat['question16'];
            $this->question17 = $objekat['question17'];
            if($objekat['question18']){
                $this->question18 = $objekat['question18'];
            }
            else {
                $this->question18 = "";
            }
            var_dump($objekat['question19']);
            foreach ($objekat['question19'] as $key => $value) {
                $this->question19 = $this->question19 . "/" . $key.'=>'.$value;
            }
            foreach ($objekat['question20'] as $key => $value) {
                $this->question20 = $this->question20 . "/" . $key.'=>'.$value;
            }
            if($objekat['question21']){
                $this->question21 = $objekat['question21'];
            }
            else {
                $this->question21 = "";
            }
            
            if($objekat['question22']){
                $this->question22 = $objekat['question22'];
            }
            else {
                $this->question22 = "";
            }
            
        }
}
class Pitanja{
	public $id_pitanje;
	public $id_anketa;
	public $pitanje;

	public function __construct($id_pitanje,$id_anketa,$pitanje){
		$this->id_anketa=$id_anketa;
		$this->id_pitanje=$id_pitanje;
		$this->pitanje=$pitanje;
	}
}

class Overa{
	public $predmet;
	public $profesor;
	public $potpis;
	public function __construct($predmet,$profesor,$potpis){
		$this->predmet=$predmet;
		$this->profesor=$profesor;
		$this->potpis=$potpis;
		
	}

}

class Ocena{
	public $predmet;
	public $ocena;
	public $id;
	public $smer;
	public $indeks;

	public function __construct($id,$indeks,$ocena,$predmet,$smer){
		$this->predmet=$predmet;
		$this->ocena=$ocena;
		$this->id=$id;
		$this->smer=$smer;
		$this->indeks=$indeks;
	}
}
class Notifikacija{
	public $id;
	public $indeks;
	public $atribut;
	public $stara_vrednost;
	public $nova_vrednost;
	public $status;

	public function __construct($id,$indeks,$atribut,$stara_vrednost,$nova_vrednost,$status){
		$this->id=$id;
		$this->indeks=$indeks;
		$this->atribut=$atribut;
		$this->stara_vrednost=$stara_vrednost;
		$this->nova_vrednost=$nova_vrednost;
		$this->status=$status;
	}
}

class OdgPrikaz
{
        public $question1;
        public $question2;
        public $question3;
        public $question4;
        public $question5;
        public $question6;
        public $question7;
        public $question8;
        public $question9;
        public $question10;
        public $question11;
        public $question12;
        public $question13;
        public $question14;
        public $question15;
        
    public function __construct($pitanje1,$pitanje2,$pitanje3,$pitanje4,$pitanje5,$pitanje6,$pitanje7,$pitanje8,$pitanje9,$pitanje10,$pitanje11,$pitanje12,$pitanje13,$pitanje14,$pitanje15)
    {
        $this->question1 = $pitanje1;
        $this->question2 = $pitanje2;
        $this->question3 = $pitanje3;
        $this->question4 = $pitanje4;
        $this->question5 = $pitanje5;
        $this->question6 = $pitanje6;
        $this->question7 = $pitanje7;
        $this->question8 = $pitanje8;
        $this->question9 = $pitanje9;
        $this->question10 = $pitanje10;
        $this->question11 = $pitanje11;
        $this->question12 = $pitanje12;
        $this->question13 = $pitanje13;
        $this->question14 = $pitanje14;
        $this->question15 = $pitanje15;
    }
}
?>