<?php
include_once 'functions.php';
session_start();
$prikazi_studenta=false;
$studenti=array();
$ime='';
if(isset($_POST['pretrazi'])){
    unset($_SESSION['studenti_svi1']);
    if($_POST['ime']!=''){
        if(!isset($_SESSION["studenti_svi1"]))
                 {
                    
                    $studenti=vrati_sve_studente($_POST['ime']);
                    
                    $_SESSION["studenti_svi1"]=$studenti;
                    $_SESSION["proba"]=$studenti;
                    if(count($studenti)==0){
                        echo '<script language="javascript">';
                        echo 'alert("Ne postoji student sa unetim imenom i prezimenom!")';
                        echo '</script>';   
                    }
                    else{
                        $prikazi_studenta=true;
                    }
                }
            else{
                $studenti=$_SESSION["studenti_svi1"];
                if(count($studenti)==0){
                        echo '<script language="javascript">';
                        echo 'alert("Ne postoji student sa unetim imenom i prezimenom!")';
                        echo '</script>';   
                    }
                    else{
                        $prikazi_studenta=true;
                    }
                }
    }
    else{
        if(empty($_POST['ime'])){
            $ime="Polje ne sme biti prazno";
        }
    }
}
if(isset($_POST['izbrisi'])){
    $studenti=$_SESSION['proba'];
        if(isset($_POST['selektuj_sve'])){
            echo count($studenti);
            for($i=0;$i<count($studenti);$i++){
                ObrisiStudenta($studenti[$i]->index);
            }
            echo "<script>window.location.href='uspesno_brisanje_studenta.php'</script>";
        }
        else{
            for($i=0;$i<count($studenti);$i++){
                if(isset($_POST[$studenti[$i]->index])){
                    ObrisiStudenta($studenti[$i]->index);
                }
            }
            echo "<script>window.location.href='uspesno_brisanje_studenta.php'</script>";
        }
}
?>
<head>        
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="icon" type="image/gif" href="Images/masm.png" />
         <link rel="shortcut icon" type="image/gif" href="Images/masm.png" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
    </head>
<body id='parent' style='background-image:url("Images/mybg.png");'>
    <div id = 'container'>  
    <div  class='container'>             
        <div  class='col-xs-12 col-sm-12 col-md-12'>
            <h1 class='jumbotron'>Pretrazite studenta za brisanje</h1>
            
                <form action='' method='post'> 
                    <div class='col-xs-10 col-sm-12 col-md-12'>
                        <div class='col-xs-10 col-sm-12 col-md-12'>
                            <label for="ime">Ime i prezime studenta:</label>
                            <input type="text" placeholder="<?php echo $ime;?>" name='ime' class="form-control" id="usr" >
                        </div>
                    </div>
                    
                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                           <br /><input  type='submit' name='pretrazi' class='btn btn-success btn-lg' value='Pretraži'>
                        </div>
                    </div>

                </form>
            </div>
            <?php
            if($prikazi_studenta){
        echo "<div class='container'>";
        echo "<div class='col-xs-12 col-sm-12 col-md-12'>";
        
        print("<form action='' method='post'>\n");
        print("<br/><label>Lista studenata:</label>\n");
        print("<table class='table'>\n");
        echo '<thead>';
        print("<tr><th>Ime</th><th>Prezime</th><th>Broj indeksa</th><th>Smer</th><th>Obeleži sve studente</th></tr>\n");
        echo '</thead>';
        echo '<tbody>';
        print("<tr><td></td><td></td><td></td><td></td><td><input type='checkbox' name='selektuj_sve' /></td></tr>\n");
        foreach($studenti as $student){
            print("<tr>\n");
            print("<td>$student->ime</td><td>$student->prezime</td><td>$student->index</td><td>$student->smer</td><td><input type='checkbox' name='$student->index'/></td>\n");
            print("</tr>\n");
        }
       
        print("<tr><td><input class='btn btn-success btn-lg' type='submit' name='izbrisi' value='Izbriši'/></td></tr>\n");
        
        echo '</tbody>';
        print("</table>\n");
        print("</form>\n");
        echo '</div>'; 
        echo "</div>";
}
        ?>
        </div>
        </div>
        
    </div>

    <script src='src/scripts.js'></script>
    <script src='src/animation.js'></script>
</body>
