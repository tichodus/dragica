<html>
    <body>       
        <!-- Modal za anektu -->
        <div id="myModal" class="modal fade in" role="dialog">
            <div class="modal-dialog">

            <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="surveyContainer"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="./src/survey.jquery.js"></script>
        <script src="./src/survey.js"></script>
    </body>
</html>