<?php
require_once 'navigation.html';

?>

<html>
    <body>
    
        <div class="container" id="cnt1">
    <div class="col-md-3">
    </div>
            <form action="" method="post">
    <div class="col-md-6" id="panel1">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">
                    <span class="fa fa-question-circle"></span>   Anketa o ocenjivanju nastave tokom semestra</h1>
            </div>
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="fa fa-question-circle"></span>   Podaci o predmetu</h3>
                    
                    <div class="panel-body two-col">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm">
                            <div class="checkbox">
                                <label>
                                    Naziv predmeta:<input type="text" id="naziv_predmeta" class="textboxovi">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm">
                            <div class="checkbox">
                                <label>
                                    Nastavnik:<input type="text" id="nastavnik" class="textboxovi">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm margin-bottom-none">
                            <div class="checkbox">
                                <label>
                                    Asistent:<input type="text" id="asistent" class="textboxovi">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
                <hr />
      
            </div>
            
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="fa fa-question-circle"></span>Iskaz o kvalitetu nastave</h3>
                    
                    <div class="panel-body two-col">
                <div class="row">
                    <div class="col-md-6">
                        <div class="well well-sm">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    Service is timely
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="well well-sm">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    Staff is friendly
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="well well-sm margin-bottom-none">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    Support is timely
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="well well-sm margin-bottom-none">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    Help resources are available
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
                <hr />
                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    Will purchase next version
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="well well-sm">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    Will purchase next version
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
            
            
            
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        <input type="submit" class="btn btn-success btn-sm btn-block" value="Predaj">
                            <span class="fa fa-send"></span>
                    </div>
                    <div class="col-md-6">
                        <input type="button" class="btn btn-primary btn-sm btn-block" value="Uradi kasnije">
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
            </form>
</div>

<style>
#cnt1 {
    background-color: rgba(215, 212, 212, 0.88);
    margin-bottom: 70px;
}

#panel1 {
    padding:20px;
}

.panel-body:not(.two-col) {
    padding: 0px;
}

.panel-body .radio, .panel-body .checkbox {
    margin-top: 0px;
    margin-bottom: 0px;
}

.panel-body .list-group {
    margin-bottom: 0;
}

.margin-bottom-none {
    margin-bottom: 0;
}
.textboxovi {
    margin-left: 10px;
}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </body>
</html>