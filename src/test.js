<?xml version="1.0" encoding="utf-8"?>
<!-- generator="Joomla! - Open Source Content Management" -->
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<title>Doktoranti van Univerziteta</title>
		<description><![CDATA[Naučne publikacije akademskog osoblja Univerziteta u Nišu]]></description>
		<link>https://www.npao.ni.ac.rs</link>
		<lastBuildDate>Mon, 05 Jun 2017 15:22:52 +0200</lastBuildDate>
		<generator>Joomla! - Open Source Content Management</generator>
		<atom:link rel="self" type="application/rss+xml" href="https://www.npao.ni.ac.rs/doktoranti-van-univerziteta?format=feed&amp;type=rss"/>
		<language>sr-yu</language>
		<item>
			<title>Aleksandar Josimovski</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1514-aleksandar-josimovski</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1514-aleksandar-josimovski</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>aleksandar_josimovski@yahoo.com (Aleksandar Josimovski)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Mon, 15 Sep 2014 00:00:00 +0200</pubDate>
		</item>
		<item>
			<title>Aleksandar Ristic</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1724-aleksandar-ristic</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1724-aleksandar-ristic</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>aleksandar.ristic@zurbnis.rs (Aleksandar Ristic)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Mon, 10 Oct 2016 00:00:00 +0200</pubDate>
		</item>
		<item>
			<title>Aleksandar Zdravkovic</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1750-aleksandar-zdravkovic</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1750-aleksandar-zdravkovic</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>zdravkovic.aleksandar87@gmail.com (Aleksandar Zdravković)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Sat, 18 Feb 2017 00:00:00 +0100</pubDate>
		</item>
		<item>
			<title>Aleksandra Kostić Tmušić</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1485-aleksandra-kostic-tmusic</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1485-aleksandra-kostic-tmusic</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>aktmusic@verat.net (Aleksandra Kostić Tmušić)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Wed, 04 Jun 2014 14:46:20 +0200</pubDate>
		</item>
		<item>
			<title>Aleksandra Marinković</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1608-aleksandra-marinkovic</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1608-aleksandra-marinkovic</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>alexytea@yahoo.com (Aleksandra Marinković)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Sat, 04 Jul 2015 00:00:00 +0200</pubDate>
		</item>
		<item>
			<title>Aleksandra Miric</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1618-aleksandra-miric</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1618-aleksandra-miric</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>aleksandramiric@yahoo.com (Aleksandra Miric)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Fri, 04 Sep 2015 00:00:00 +0200</pubDate>
		</item>
		<item>
			<title>Aleksandra Šafranj</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1320-aleksandra-safranj</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1320-aleksandra-safranj</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>safran@sezampro.rs (Aleksandra Šafranj)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Wed, 26 Feb 2014 14:20:19 +0100</pubDate>
		</item>
		<item>
			<title>Amela Malićević</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1430-amela-malicevic</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1430-amela-malicevic</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>amela.malicevic@gmail.com (Amela Malićević)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Tue, 25 Mar 2014 14:27:39 +0100</pubDate>
		</item>
		<item>
			<title>Amela Malićević</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1431-amela-malicevic</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1431-amela-malicevic</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>amela.malicevic@gmail.com (Amela Malićević)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Tue, 25 Mar 2014 14:27:39 +0100</pubDate>
		</item>
		<item>
			<title>Ana Daneva - Markova</title>
			<link>https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1437-ana-daneva-markova</link>
			<guid isPermaLink="true">https://www.npao.ni.ac.rs/doktoranti-van-univerziteta/1437-ana-daneva-markova</guid>
			<description><![CDATA[<div class="K2FeedIntroText"></div>]]></description>
			<author>anadaneva@yahoo.com (Ana Daneva - Markova)</author>
			<category>Doktoranti van Univerziteta</category>
			<pubDate>Thu, 27 Mar 2014 18:56:05 +0100</pubDate>
		</item>
	</channel>
</rss>