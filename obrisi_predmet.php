<?php
include_once 'functions.php';
session_start();
$prikazi_predmet=false;
$predmeti=array();
$naziv='';
if(isset($_POST['pretrazi'])){
    unset($_SESSION['ppp']);
    if($_POST['predmet']!=''){
        if(!isset($_SESSION["ppp"]))
                 {
                    $predmeti=vrati_predmet_po_nazivu($_POST['predmet']);
                    $_SESSION["ppp"]=$predmeti;
                    $_SESSION["proba111"]=$predmeti;
                    if(count($predmeti)==0){
                        echo '<script language="javascript">';
                        echo 'alert("Ne postoji predmet sa unetim nazivom!")';
                        echo '</script>';   
                    }
                    else{
                        $prikazi_predmet=true;
                    }
                }
            else{
                $predmeti=$_SESSION["ppp"];
                if(count($predmeti)==0){
                        echo '<script language="javascript">';
                        echo 'alert("Ne postoji predmet sa unetim nazivom!")';
                        echo '</script>';   
                    }
                    else{
                        $prikazi_predmet=true;
                    }
                }
    }
    else{
        if(empty($_POST['predmet'])){
            $naziv="Polje ne sme biti prazno";
        }
    }
}
if(isset($_POST['izbrisi'])){
    $predmeti=$_SESSION['proba111'];
    //var_dump($predmeti);
        if(isset($_POST['selektuj_sve'])){
            //echo count($studenti);
            for($i=0;$i<count($predmeti);$i++){
                ObrisiPredmet($predmeti[$i]->id);
            }
            echo "<script>window.location.href='uspesno_brisanje_predmeta.php'</script>";
        }
        else{
            for($i=0;$i<count($predmeti);$i++){
                if(isset($_POST[$predmeti[$i]->id])){
                    ObrisiPredmet($predmeti[$i]->id);
                }
            }
            echo "<script>window.location.href='uspesno_brisanje_predmeta.php'</script>";
        }
}

?>
<head>        
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="icon" type="image/gif" href="Images/masm.png" />
         <link rel="shortcut icon" type="image/gif" href="Images/masm.png" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
    </head>
<body id='parent' style='background-image:url("Images/mybg.png");'>
    <div id = 'container'>  
        <div class='col-xs-12 col-sm-12 col-md-12'>
           
            <div class='container'>
                <h1 class='jumbotron'>Brisanje predmeta iz sistema</h1>
                <form action='' method='post'>    

                    <div class='col-xs-12 col-sm-12 col-md-12'>
                        <div class='col-xs-12 col-sm-12 col-md-12'>
                            <label for="usr">Naziv predmeta:</label>
                            <input type="text" placeholder="<?php echo $naziv;?>" name='predmet' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        <div class='col-xs-12 col-sm-6 col-md-6'>
                        <br /><input  type='submit' class='btn btn-success btn-lg' name='pretrazi' class='form-control' value='Pretraži'>
                        </div>
                    </div>

                </form>
                <?php
                    if($prikazi_predmet){
                    echo "<div class='co-xs-12 col-sm-12 col-md-12'>";
                    print("<form action='' method='post'>\n");
                    echo "<div class='table-responsive'>";
                    print("<table class='table'>\n");
                    echo '<thead>';
                    print("<tr><th>Naziv predmeta</th><th>Espb</th><th>Tip</th><th>Smer</th><th>Godina</th><th></th></tr>\n");
                    echo '</thead>';
                    echo '<tbody>';
                    print("<tr><td></td><td></td><td></td><td></td><td></td><td><input type='checkbox' name='selektuj_sve' /></td></tr>\n");
                    foreach($predmeti as $predmet){
                        print("<tr>\n");
                        print("<td>$predmet->naziv</td><td>$predmet->espb</td><td>$predmet->obavezni</td><td>$predmet->smer</td><td>$predmet->godina</td><td><input type='checkbox' name='$predmet->id'/></td>\n");
                        print("</tr>\n");
                    }
                    print("<tr><td><input class='btn btn-success btn-lg' type='submit' name='izbrisi' value='Izbriši'/></td><td></td></tr>\n");
                    echo '</tbody>';
                    print("</table>\n");
                    echo "</div>";
                    print("</form>\n");
                    echo "</div>";
                    
}
                ?>
            </div>
        </div>
        
        
    </div>

    <script src='src/scripts.js'></script>
    <script src='src/animation.js'></script>
</body>
