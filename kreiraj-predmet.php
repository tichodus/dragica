<?php
require_once 'functions.php';
$naziv='';
$espb='';
$obavezni='';
$smer='';
$semestar='';
$opis='';

if(isset($_POST['potvrdi'])){
    if($_POST['naziv']!='' && $_POST['espb']!='' && $_POST['tip']!='' && $_POST['smer']!='' && $_POST['semestar']!=''){
        if($_POST['semestar']==1 || $_POST['semestar']==2){
            if((strcmp($_POST['smer'],"None")))
            {
                echo '<script language="javascript">';
                echo 'alert("Za prvi i drugi semestar se ne bira smer!")';
                echo '</script>';
            }
            else{
                createPredmet();
                echo "<script>window.location.href='uspesno_kreiranje_predmeta.php'</script>";
            }
        }
        else if($_POST['semestar']!=1 || $_POST['semestar']!=2){
                if(!(strcmp($_POST['smer'],"None")))
            {
                echo '<script language="javascript">';
                echo 'alert("Niste izabrali smer!")';
                echo '</script>';
            }
            else{
                createPredmet();
                echo "<script>window.location.href='uspesno_kreiranje_predmeta.php'</script>";
            }
            }
        else{
                createPredmet();
                echo "<script>window.location.href='uspesno_kreiranje_predmeta.php'</script>";

            }
    }
    else{
        echo "mxsmx,s.mc,";
        if(empty($_POST['naziv'])){
            $naziv="Polje ne sme biti prazno";
        }
        if(empty($_POST['espb'])){
            $espb='Polje ne sme biti prazno';
        }
        if(empty($_POST['tip'])){
            $obavezni="Polje ne sme biti prazno";
        }
        if(empty($_POST['smer'])){
            $smer="Polje ne sme biti prazno";
        }
        if(empty($_POST['semestar'])){
            $semestar="Polje ne sme biti prazno";
        }
    }
}

?>
<head>        
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="icon" type="image/gif" href="Images/masm.png" />
         <link rel="shortcut icon" type="image/gif" href="Images/masm.png" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
    </head>
<body id='parent' style='background-image:url("Images/mybg.png");'>
    <div  class='container'>
        <h1 class='jumbotron'>Kreiraj novi predmet</h1>   
                     
        <div class='col-xs-12 col-sm-12 col-md-12'>
           
            
                <form action='' method='post'>    

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                            <label for="usr">Naziv predmeta:</label>
                            <input type="text" placeholder="<?php echo $naziv;?>" name='naziv' class="form-control" id="usr" >
                        
                    </div>
                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                            <br />
                            <label for="usr">Semestar u kom se predmet sluša:</label>
                            <select class='form-control' name='semestar'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                        </select>
                        
                    </div>

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                       
                            <label for="usr">Espb bodovi:</label>
                            <input type="text" placeholder="<?php echo $espb;?>" name='espb' class="form-control" id="usr" >
                      
                    </div>

                     <div class='col-xs-12 col-sm-6 col-md-6'>
                       
                            <label for="usr">Smer:</label>
                            <select class='form-control' name='smer'>
                                <option value="None" name="none">None</option>
                                <option value="Elektroenergetika">Elektroenergetika</option>
                                <option value="Elektronske komponente i mikrosistemi">Elektronske komponente i mikrosistemi</option>
                                <option value="Racunarstvo i informatika">Racunarstvo i informatika</option>
                                <option value="Upravljanje sistemima">Upravljanje sistemima</option>
                                <option value="Elektronika i mikroprocesorska tehnika">Elektronika i mikroprocesorska tehnika</option>
                                 <option value="Telekomunikacije">Telekomunikacije</option>
                            </select>
                        
                    </div>

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                       
                            <label for="usr">Tip predmeta:</label>
                            <select name='tip' class="form-control">
                                <option value="O" name="O">Obavezni</option>
                                <option value="I" name="I">Izborni</option>
                            </select>
                       
                    </div>

                     <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                            <label for="usr">Opis predmeta:</label>
                            <input type="text" name='opis' class="form-control" id="usr" >
                        
                    </div>

                    <div class='col-xs-12 col-sm-12 col-md-12'>
                        
                       <br/> <input  type='submit' name='potvrdi' class='btn btn-success btn-lg' value='Potvrdi'>
                        
                    </div>

                </form>
            </div>
        </div><br/>
        
        
   

    <script src='src/scripts.js'></script>
    <script src='src/animation.js'></script>
</body>