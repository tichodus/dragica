<?php
    require_once 'style.html';
    include_once 'functions.php';
    include_once 'classes.php';
    session_start();
    $korisnik=$_SESSION['korisnik'];

    $lista_predmeta=false;
    $lista_studenata=false;
    $predmeti=array();
    $studenti=array();
    $predmeti_izmeni=array();
    $studenti_izmeni=array();
    if(isset($_POST['izaberi'])){
        $_SESSION['predmeti_izmeni']=vrati_predmete($_POST['smer'],$_POST['godina'],$korisnik->id);
         if($_POST['godina']==1)
        {
            if((strcmp($_POST['smer'],"None")))
            {
                echo '<script language="javascript">';
                echo 'alert("Za prvu godinu studija se ne bira smer!")';
                echo '</script>';
            }
            else{
                 $lista_predmeta=true;
                if(!isset($_SESSION["predmeti"]))
                 {
                    $predmeti=vrati_predmete($_POST['smer'],$_POST['godina'],$korisnik->id);
                    $_SESSION["predmeti"]=$predmeti;
                }
            else{
                    $predmeti=$_SESSION["predmeti"];
                }
            }
        }
        else if($_POST['godina']!=1){
            if(!(strcmp($_POST['smer'],"None")))
            {
                echo '<script language="javascript">';
                echo 'alert("Morate da izaberete jedan od ponudjenih smerova!")';
                echo '</script>';
            }
             else{
                 $lista_predmeta=true;
                if(!isset($_SESSION["predmeti"]))
                 {
                    $predmeti=vrati_predmete($_POST['smer'],$_POST['godina'],$korisnik->id);
                    $_SESSION["predmeti"]=$predmeti;
                }
            else{
                    $predmeti=$_SESSION["predmeti"];
                }
            }
        }
         else{
                 $lista_predmeta=true;
                if(!isset($_SESSION["predmeti"]))
                 {
                    $predmeti=vrati_predmete($_POST['smer'],$_POST['godina'],$korisnik->id);
                    $_SESSION["predmeti"]=$predmeti;
                }
            else{
                    $predmeti=$_SESSION["predmeti"];
                }
            }
            unset($_SESSION['predmeti']);
    }
    if(isset($_POST['potvrdi'])){
        $lista_studenata=true;
        $predmeti_izmeni=$_SESSION['predmeti_izmeni'];
        for($i=0;$i<count($predmeti_izmeni);$i++)
        {
        if (isset($_POST[$predmeti_izmeni[$i]->id]))
        {
            $_SESSION['prosledi']=$predmeti_izmeni[$i]->id;
            if(!isset($_SESSION["studenti"]))
            {
            $studenti=vrati_nepotpisane_studente($predmeti_izmeni[$i]->id);
            $_SESSION["studenti"]=$studenti;
            $_SESSION["studenti_izmeni"]=$studenti;
            }
            else{
                $studenti=$_SESSION["studenti"];
            }
        }
        }
        unset($_SESSION['studenti']);
    }
  
    if(isset($_POST['prosledi'])){
        $predmet=$_SESSION['prosledi'];
        $studenti_izmeni=$_SESSION["studenti_izmeni"]; 
        if(isset($_POST['selektuj_sve'])){
            for($i=0;$i<count($studenti_izmeni);$i++){
                potpis($studenti_izmeni[$i]->index,$predmet);
            }
        }
        else{
            for($i=0;$i<count($studenti_izmeni);$i++){
                if(isset($_POST[$studenti_izmeni[$i]->index])){
                    potpis($studenti_izmeni[$i]->index,$predmet);
                }
            }
        }
    }
    
    if($lista_predmeta){
         echo "<div class='container'>";
        echo "<div class='col-xs-12 col-sm-12 col-md-12'>";
       
        print("<form action='' method='post'>\n");
        print("<label>Izaberite predmet:</label>\n");
        echo "<div class='table-responsive'>";
        print("<table class='table'>\n");
        echo '<thead>';
        print("<tr><th>Naziv predmeta</th><th></th></tr>\n");
        echo '</thead>';
        echo '<tbody>';
        foreach($predmeti as $predmet){
            print("<tr>\n");
            print("<td>$predmet->naziv</td><td><input type='checkbox'  name='$predmet->id'/></td>\n");
            print("</tr>\n");
        }
        print("<tr><td><input class='btn btn-success btn-lg' type='submit' name='potvrdi' value='Potvrdi'/></td><td></td></tr>\n");
        echo '</tbody>';
        print("</table>\n");
        print("</form>\n");
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
    if($lista_studenata){
        echo "<div class='container'>";
        print("<form action='' method='post'>\n");
        print("<label>Studenti koji slušaju odabrani predmet:</label>\n");
        print("<table class='table'>\n");
        echo '<thead>';
        print("<tr><th>Ime</th><th>Prezime</th><th>Broj indeksa</th><th>Smer</th><th>Obeleži sve studente</th></tr>\n");
        echo '</thead>';
        echo '<tbody>';
        print("<tr><td></td><td></td><td></td><td></td><td><input type='checkbox' name='selektuj_sve' /></td></tr>\n");
        foreach($studenti as $student){
            print("<tr>\n");
            print("<td>$student->ime</td><td>$student->prezime</td><td>$student->index</td><td>$student->smer</td><td><input type='checkbox' name='$student->index'/></td>\n");
            print("</tr>\n");
        }
        print("<tr><td><input class='btn btn-success btn-lg' type='submit' name='prosledi' value='Prosledi'/></td><td></td></tr>\n");
        echo '</tbody>';
        print("</table>\n");
        print("</form>\n");
        echo '</div>';
    }
   
?>
<body id='parent' style='background-image:url("Images/mybg.png");'>
     <div class='col-xs-12 col-sm-12 col-md-12'>
    <div id='container' class='container'>
    <div class='col-xs-12 col-sm-12 col-md-12'>
        <h1 class='jumbotron'>Davanje potpisa</h1>
    </div>

    </h1>
    <form action='' method="post">
    
    <div class='col-xs-12 col-sm-12 col-md-12'>
    <div class='col-xs-12 col-sm-12 col-md-12'>
        <label for='smer'>Izaberi smer:</label>
        <select class='form-control' name='smer'>
            <option value="None" name="none">None</option>
            <option value="Elektroenergetika">Elektroenergetika</option>
            <option value="Elektronske komponente i mikrosistemi">Elektronske komponente i mikrosistemi</option>
            <option value="Racunarstvo i informatika">Racunarstvo i informatika</option>
            <option value="Upravljanje sistemima">Upravljanje sistemima</option>
            <option value="Elektronika i mikroprocesorska tehnika">Elektronika i mikroprocesorska tehnika</option>
            <option value="Telekomunikacije">Telekomunikacije</option>
        </select>
    </div>
    </div>
    <div>
    
    <div class='col-xs-12 col-sm-12 col-md-12'>
    <div class='col-xs-12 col-sm-12 col-md-12'>
        <label for='name'>Izaberi godinu:</label>
        <select id='name'class='form-control' name='godina'>
            <option value="1" name="1">1</option>
            <option value="2" name="2">2</option>
            <option value="3" name="3">3</option>
            <option value="4" name="4">4</option>
            <option value="5" name="5">5</option>
        </select>
    </div>
    </div>
    <div class='col-md-12'>
        <div class='col-xs-8 col-sm-8 col-md-8'>
            <br />
            <input type='submit' class='btn btn-success btn-lg' name='izaberi' value='Izaberi' />
        </div>
    </div>
    </div>
    </form>
    </div>
    </div><br/>
    <script src='src/animation.js'></script>
</body>
