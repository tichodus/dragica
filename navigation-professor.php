<html>
    <head>
        <title>Dragica</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="icon" type="image/gif" href="Images/masm.png" />
         <link rel="shortcut icon" type="image/gif" href="Images/masm.png" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
    </head>

    <body>
        
        <nav class='sideNav' id='sideNav'>
            <ul>
                <li><a style='margin-top:5em;' id='pocetna' href='#'>Pocetna</a></li>
                <li><a  id='profil' href='#'>Profil</a></li>
                <li><a  href='#' id='ocenjivanje'>Ocenjivanje</a></li>
                <li><a  href='#' id='predmeti'>Spisak vasih predmeta</a></li>
                
            </ul>
        </nav>
         
<script>
     function closeNav(){
       document.getElementById('sideNav').style.width='0';
       document.body.style.backgroundColor ="white"
       document.getElementById('container').style.marginLeft='0';
       document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-right';
    }
    function openNav(event){
       var screen_width = $(window).width(); 
            if(screen_width<=981){
                var x = event.clientX;
                var width = document.getElementById('sideNav').style.width;
            if(width == '100%'){
                    closeNav();
                    
            }
        else{
       
        document.getElementById('sideNav').style.width='100%';
        document.getElementById('container').style.marginLeft='250px';
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-left';
        console.log(document.getElementById('nav-span').style);
       }
            }
      else{
       var x = event.clientX;
       var width = document.getElementById('sideNav').style.width;
       if(width == '250px'){
            closeNav();
            document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-right';
       }
        else{
       
        document.getElementById('sideNav').style.width='250px';
        document.getElementById('container').style.marginLeft='250px';
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-left';
        console.log(document.getElementById('nav-span').style);
       }
     }
    }
</script>
<script src='src/scripts-profesor.js'></script>



        
    </body>
</html>