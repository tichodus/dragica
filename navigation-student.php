<?php
    
    $overen = false;
    if(isset($_SESSION['overa']))
        $overen = $_SESSION['overa'];
    
    $isValid = false;
    $today = date('Y-m-d');
    $date1= date('Y-01-03');
    $date2= date('Y-01-10');
    $date3= date('Y-06-03');
    $date4= date('Y-06-10');

    if($today>$date1 && $today<$date2 || $today>$date3 && $today<$date4)
        $isValid=true;

    

?>


<html>
    <head>
        <title>Dragica</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="icon" type="image/gif" href="Images/masm.png" />
         <link rel="shortcut icon" type="image/gif" href="Images/masm.png" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
        
    </head>

    <body onmousemove="">
       
        <nav class='sideNav' id='sideNav'>
            
            <ul>
                <li><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a></li>
                <li><a style='margin-top:5em;' id='pocetna' href='#'>Pocetna</a></li>
                <li><a href='#'  id='profil' name='profil-student.php'>Profil</a></li>
                <li><a href='#' <?php if(!$isValid) echo "style='display:none;";?>  id='overa'>Overa</a></li>
                <li><a href='#'  id='ocene' name='ocene.php'>Pregled ocena</a></li>
                <li><a href='#'  id='predmeti' name='predmeti.php'>Pregled predmeta</a></li>
                <li><a href='#'  <?php if(!$overen) echo "style='display:none;"?> id='upis' name='upis.php'>Upis semestra</a></li>
                
                
            </ul>
        </nav>

        
         
<script>
    function closeNav(){
       document.getElementById('sideNav').style.width='0';
       document.body.style.backgroundColor ="white"
       document.getElementById('container').style.marginLeft='0';
       document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-right';
    }
    function openNav(event){
       var screen_width = $(window).width(); 
            if(screen_width<=981){
                var x = event.clientX;
                var width = document.getElementById('sideNav').style.width;
            if(width == '100%'){
                    closeNav();
                    
            }
        else{
       
        document.getElementById('sideNav').style.width='100%';
        document.getElementById('container').style.marginLeft='250px';
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-left';
        console.log(document.getElementById('nav-span').style);
       }
            }
      else{
       var x = event.clientX;
       var width = document.getElementById('sideNav').style.width;
       if(width == '250px'){
            closeNav();
            document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-right';
       }
        else{
       
        document.getElementById('sideNav').style.width='250px';
        document.getElementById('container').style.marginLeft='250px';
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        document.getElementById('nav-span').className='	glyphicon glyphicon-chevron-left';
        
       }
     }
    }

</script>
<script src='src/scripts.js'></script>



        
    </body>
</html>