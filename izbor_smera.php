<?php
    require_once 'functions.php';
    require_once 'style.html';
    
    if(isset($_GET['izaberi-btn']))
    {
        upisiSmer($_SESSION['korisnik'],$_GET['smer']);
        $korisnik = vratiStudenta($_SESSION['korisnik']->index);        
        $_SESSION['korisnik'] = $korisnik;
        echo "<script>window.location.href='student.php'</script>";
    }
?>

<body  id='parent' style="background-image:url('Images/mybg.png');" class=''>
<div  class='container'>
    <div class='col-xs-12 col-sm-12 col-md-12'>
        <h1 class='jumbotron'>Izaberite smer koji želite da upišete</h1>
    </div>
        <div class='col-xs-12 col-sm-12 col-md-12'>
            <div class='col-xs-10 col-sm-8 col-md-8'>
                <label for="usr">Smer:</label>
                <form action='' method='get'>
                    <select name='smer' class='form-control' id='usr'>
                        <option value='Energetika'>Energetika</option>
                        <option value='Elektronika i mikroprocesorska tehnika'>Elektronika i mikroprocesorska tehnika</option>
                        <option value='Elektronske komponenete i mikrosistemi'>Elektronske komponenete i mikrosistemi</option>
                        <option value='Računarstvo i informatika'>Racunarstvo i informatika</option>
                        <option value='Upravljanje sistemima'>Upravljanje sistemima</option>
                        <option value='Telekomunikacije'>Telekomunikacije</option>
                    </select><br />
                    <input type='submit' class='btn btn-success btn-lg' value='Izaberi' name='izaberi-btn' />
                </form>
            </div>
        </div>
</div>
<script src='src/animation.js'></script>
</body>