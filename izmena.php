<?php
    require_once 'style.html';
    require_once 'functions.php';
    session_start();
    $korisnik=$_SESSION['korisnik'];
 
    $modul = '';
    $smer='';
    $ime_roditelja = '';
    $ime = '';
    $prezime = '';
    $mesto_rodjenja = '';
    $adresa = '';
    $grad = '';
    $drzava = '';
    $mesto_stanovanja = '';
    $broj_telefona = '';
    $drzavljanstvo = '';
    $skolska_sprema = '';
    $poteskoce = '';
    $nacionalna_pripadnost='';
    if(isset($_GET['potvrdi']) )
    {
        if($_GET['modul']!='' && $_GET['smer']!='' && $_GET['ime_roditelja']!='' && $_GET['ime']!='' && $_GET['prezime']!='' && $_GET['mesto_rodjenja']!='' && $_GET['adresa']!='' && $_GET['grad']!='' && $_GET['poteskoce']!='' && $_GET['drzava']!='' && $_GET['mesto_stanovanja']!='' && $_GET['broj_telefona']!='' && $_GET['drzavljanstvo']!='' && $_GET['skolska_sprema']!='' && $_GET['nacionalna_pripadnost']!=''){
                sendNotifications();
                echo ("<script>window.location.href='notifikacije_poslate.php';</script>");
            }
        else{
         if(empty($_GET['modul']))
            $modul='Polje ne sme biti prazno';
         if(empty($_GET['ime_roditelja']))
            $ime_roditelja='Polje ne sme biti prazno';
         if(empty($_GET['ime']))
            $ime='Polje ne sme biti prazno';
         if(empty($_GET['prezime']))
            $prezime='Polje ne sme biti prazno';
         if(empty($_GET['mesto_rodjenja']))
            $mesto_rodjenja='Polje ne sme biti prazno';
         if(empty($_GET['adresa']))
            $adresa='Polje ne sme biti prazno';
         if(empty($_GET['grad']))
            $grad='Polje ne sme biti prazno';
         if(empty($_GET['poteskoce']))
            $poteskoce='Polje ne sme biti prazno';
         if(empty($_GET['drzava']))
            $drzava='drzava ne sme biti prazno';
         if(empty($_GET['mesto_stanovanja']))
            $mesto_stanovanja='Polje ne sme biti prazno';
         if(empty($_GET['broj_telefona']))
            $broj_telefona='Polje ne sme biti prazno';
         if(empty($_GET['drzavljanstvo']))
            $drzavljanstvo='Polje ne sme biti prazno';
         if(empty($_GET['skolska_sprema']))
            $skolska_sprema='Polje ne sme biti prazno';
         if(empty($_GET['nacionalna_pripadnost']))
            $nacionalna_pripadnost='Polje ne sme biti prazno';
        }
    }
    
?>

<body style="background-image:url('Images/mybg.png');" class=''>
<div id = 'parent'>
         <header>
             <h1>Izmeni lične podatke</h1>
         </header>   
                     
        <div  style='  width:80%;  margin-left:10%; padding-left:40px;'>
           
            <div class='row'>
                <form action='izmena.php' method='get'>
                    <div id='broj_indeksa' class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Br. Indeksa:</label>
                            <label for="usr" name="indeks" value="<?php echo $korisnik->index ?>"> <?php echo $korisnik->index ?> </label>
                        </div>
                    </div>

                    
                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">JMBG:</label>
                            <label for="usr" > <?php echo $korisnik->jmbg ?> </label>
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Modul:</label>
                            <input type="text" placeholder="<?php if($modul!="") echo $modul;?>" value="<?php if ($modul=="")echo $korisnik->modul;?>" name='modul' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Smer:</label>
                            <input type="text" placeholder="<?php if($smer!="")echo $smer;?>" value="<?php if($korisnik->smer=="none") echo "Opsti"; else echo $korisnik->smer;?>" name='smer' class="form-control" id="usr" >
                        </div>
                    </div>

                    

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Ime:</label>
                            <input type="text" placeholder="<?php if($ime!="")echo $ime;?>" value="<?php if($ime=="")echo $korisnik->ime?>" name='ime' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Ime roditelja:</label>
                            <input type="text" placeholder="<?php if($ime_roditelja!="")echo $ime_roditelja;?>" value="<?php if($ime_roditelja=="")echo $korisnik->ime_roditelja;?>" name='ime_roditelja' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Prezime:</label>
                            <input type="text" placeholder="<?php if($prezime!="")echo $prezime;?>" value="<?php if($prezime=="")echo $korisnik->prezime;?>" name='prezime' class="form-control" id="usr" >
                        </div>
                    </div>


                     <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                             <br/><label for="usr">Pol:</label>
                            <div class="radio">
                                <label><input type="radio" value='m'  name="pol" <?php if($korisnik->pol=='m') echo 'checked';?>>Muski</label>
                            </div>
                            <div class='radio'>
                                <label><input type="radio" value='z' name="pol" <?php if($korisnik->pol=='z') echo 'checked';?> >Zenski</label>
                            </div>
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8 '>
                            <label for="usr">Godina rodjenja:</label>
                            <label for="usr" name="datum_rodjenja" value="<?php echo $korisnik->god_rodjenja ?>"> <?php echo $korisnik->god_rodjenja ?> </label>
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Mesto rodjenja:</label>
                            <input type="text" placeholder="<?php if($mesto_rodjenja!="")echo $mesto_rodjenja;?>" value="<?php if($mesto_rodjenja=="")echo $korisnik->mesto_rodjenja;?>" name='mesto_rodjenja' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Adresa:</label>
                            <input type="text" placeholder="<?php if($adresa!="")echo $adresa;?>"value="<?php if($adresa=="")echo $korisnik->adresa;?>" name='adresa' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Grad:</label>
                            <input type="text" placeholder="<?php if($grad!="")echo $grad;?>" value="<?php if($grad=="")echo $korisnik->grad;?>" name='grad' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Drzava:</label>
                            <input type="text" placeholder="<?php if($drzava!="")echo $drzava;?>" value="<?php if($drzava=="") echo $korisnik->drzava;?>" name='drzava' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Mesto stanovanja:</label>
                            <input type="text" placeholder="<?php if($mesto_stanovanja!="")echo $mesto_stanovanja;?>" value="<?php if($mesto_stanovanja=="")echo $korisnik->mesto_stanovanja;?>" name='mesto_stanovanja' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="phone">Pozivni za broj telefona:</label>
                            <input type="number"  name='broj_telefona_pozivni' onclick="Phone()" style='width: 90px;' onkeydown='return false' value=<?php echo substr($korisnik->telefon,0,3);?> min='060' max='069' class="form-control" id="phone" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="phone1">Broj telefona:</label>
                            <input minlength="6" placeholder="<?php if($broj_telefona!="")echo $broj_telefona;?>" value="<?php if($broj_telefona=="")echo substr($korisnik->telefon,3);?>" maxlength="7" type="text" name='broj_telefona' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" id="phone1" >
                        </div>
                    </div>
                    
                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Drzavljanstvo:</label>
                            <input type="text" placeholder="<?php if($drzavljanstvo!="")echo $drzavljanstvo;?>" value="<?php if($drzavljanstvo=="")echo $korisnik->drzavljanstvo;?>" name='drzavljanstvo' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Nacionalna pripadnost:</label>
                            <input type="text" placeholder="<?php if($nacionalna_pripadnost!="")echo $nacionalna_pripadnost;?>" value="<?php if($nacionalna_pripadnost=="")echo $korisnik->nacionalnost;?>" name='nacionalna_pripadnost' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-12 col-md-12'>
                        <div class='col-xs-10 col-sm-4 col-md-4'>
                            <label for="usr">Skolska sprema:</label>
                            <input type="text" placeholder="<?php if($skolska_sprema!="")echo $skolska_sprema;?>" value="<?php if($skolska_sprema=="")echo $korisnik->skolovanje;?>" name='skolska_sprema' class="form-control" id="usr" >
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Skolska sprema oca:</label>

                            <div class="radio">
                                <label><input type="radio" value='niza' name="sprema_oca" <?php if($korisnik->obrazovanje_oca=='niza') echo 'checked';?>>Niza strucna sprema</label>
                            </div>
                            <div class='radio'>
                                <label><input type="radio" value='srednja' name="sprema_oca" <?php if($korisnik->obrazovanje_oca=='srednja') echo 'checked';?>>Srednja strucna sprema</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value='visoka' name="sprema_oca" <?php if($korisnik->obrazovanje_oca=='visoka') echo 'checked';?>>Visa strucna sprema/Fakultet</label>
                            </div>
                            <div class='radio'>
                                <label><input type="radio" value='magistratura' name="sprema_oca" <?php if($korisnik->obrazovanje_oca=='magistratura') echo 'checked';?>>Magistratura</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value='doktorat' name="sprema_oca" <?php if($korisnik->obrazovanje_oca=='doktorat') echo 'checked';?> >Doktorat</label>
                            </div>
                            
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Skolska sprema majke:</label>
                            <div class="radio">
                                <label><input type="radio" value='niza' name="sprema_majke" <?php if($korisnik->obrazovanje_majke=='niza') echo 'checked';?>>Niza strucna sprema</label>
                            </div>
                            <div class='radio'>
                                <label><input type="radio" value='srednja' name="sprema_majke" <?php if($korisnik->obrazovanje_majke=='srednja') echo 'checked';?> >Srednja strucna sprema</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value='visoka' name="sprema_majke" <?php if($korisnik->obrazovanje_majke=='visoka') echo 'checked';?> >Visa strucna sprema/Fakultet</label>
                            </div>
                            <div class='radio'>
                                <label><input type="radio" value='magistratura' name="sprema_majke" <?php if($korisnik->obrazovanje_majke=='magistratura') echo 'checked';?> >Magistratura</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" value='doktorat' name="sprema_majke" <?php if($korisnik->obrazovanje_majke=='doktorat') echo 'checked';?> >Doktorat</label>
                            </div>
                            
                        </div>
                    </div>

                    <div class='col-xs-10 col-sm-6 col-md-6'>
                        <div class='col-xs-10 col-sm-8 col-md-8'>
                            <label for="usr">Ukoliko student ima ikakvih poteskoca, neka ih navede:</label>
                            <input type="text" placeholder="<?php if($poteskoce!="")echo $poteskoce;?>" value="<?php if($poteskoce=="")echo $korisnik->poteskoce;?>" name='poteskoce' class="form-control" id="usr" >
                        </div>
                    </div>
                    

                    
                    <div class='col-xs-10 col-sm-8 col-md-8'>
                        
                        <input  id="potvrdi" type='submit' style=' background-color:lightgreen; margin-top:1em; margin-bottom:1em; color:white;' name='potvrdi' class='form-control' value='Potvrdi'>
                        
                    </div>

                </form>
            </div>
        </div>
        
    </div>

    <script src='src/animation.js'></script>
    </body>
