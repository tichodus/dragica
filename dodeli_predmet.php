<?php
require_once 'functions.php';
session_start();
$profesor1='';
$predmet1='';
$lista=false;
$profesori=array();
$predmeti=array();
error_reporting(E_ALL & ~E_NOTICE);
if(isset($_POST['potvrdi'])){
    if($_POST['profesor_ime']!='' && $_POST['predmet_naziv']!=''){
        $profesori=vrati_sve_profesore1();
        $_SESSION['prof']=$profesori;
        $predmeti=vrati_predmet_po_nazivu($_POST['predmet_naziv']);
         $_SESSION['pred']=$predmeti;
        if($profesori!=null && $predmeti!=null){
            $lista=true;
        }
        else{
                echo '<script language="javascript">';
                echo 'alert("Niste uneli postojeci predmet ili profesora!")';
                echo '</script>';
        }
    }
    else{
        if(empty($_POST['profesor_ime'])){
            $profesor1="Polje ne sme biti prazno";
        }
        if(empty($_POST['predmet_naziv'])){
            $predmet1='Polje ne sme biti prazno';
        }
    
    }
}
if(isset($_POST['prosledi'])){
    $profesori=$_SESSION['prof'];
    $predmeti=$_SESSION['pred'];
    $prof=0;
    $pred=0;
        for($i=0;$i<count($profesori);$i++)
        { 
            if (isset($_POST[$profesori[$i]->id]))
            {
                $prof=$profesori[$i]->id;
                
            }
            if($prof == 0)
            {
                echo "Niste izabrali profesora!";
            }
            if($_POST['check'])
               {
                foreach($_POST['check'] as $checkbox)
                {
                    if($prof!=0)
                    dodeliPredmet($checkbox,$prof);
                }
                echo "<script>window.location.href='uspesna_dodela_predmeta.php'</script>";
               }
               else{
                   echo "<label class='alert alert-danger'>Niste izabrali predmet!</label>";
               }
        if($br!=0){}
        }
}


?>
<head>        
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="icon" type="image/gif" href="Images/masm.png" />
         <link rel="shortcut icon" type="image/gif" href="Images/masm.png" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
    </head>
<body id='parent' style='background-image:url("Images/mybg.png");'>
    <div class = 'container'>
        
             <h1 class='jumbotron'>Dodeli predmet profesoru</h1>
          
                     
        <div class='col-xs-12 col-sm-12 col-md-12'>
           
            
                <form action='' method='post'>    

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                            <label for="usr">Profesor:</label>
                            <input type="text" placeholder="<?php echo $profesor1;?>" name='profesor_ime' class="form-control" id="usr" >
                        
                    </div>

                    <div class='col-xs-12 col-sm-6 col-md-6'>
                        
                            <label for="usr">Naziv predmeta:</label>
                            <input type="text" placeholder="<?php echo $predmet1;?>" name='predmet_naziv' class="form-control" id="usr" >
                        
                    </div>

                    <div class='col-xs-12 col-sm-12 col-md-12'>
                        
                        <br/><input  type='submit'  name='potvrdi' class='btn btn-success btn-lg' value='Potvrdi'>
                        
                    </div>

                </form>
                <?php
                    if($lista){
        echo "<div class='container'>";
        echo "<div class='col-xs-12 col-sm-12 col-md-12'>";
        print("<form action='' method='post'>\n");
        
        print("<table class='table'>\n");
        echo '<thead>';
        print("<tr><th>Ime</th><th>Zvanje</th><th></th><th></th><th></th><th></th></tr>\n");
        echo '</thead>';
        echo '<tbody>';
        foreach($profesori as $profesor){
            print("<tr>\n");
            print("<td>$profesor->ime</td><td>$profesor->zvanje</td><td><td><td><td><td><td><input align='right' type='checkbox' name='$profesor->id'/></td></td></td></td></td></td>\n");
            print("</tr>\n");
        }
        echo '</tbody>';
        print("</table>\n");
        print("<label>Lista predmeta sa unetim nazivom:</label>\n");
        print("<table class='table'>\n");
        echo '<thead>';
        print("<tr><th>Naziv predmeta</th><th>Espb</th><th>Tip</th><th>Smer</th><th>Godina</th><th></th></tr>\n");
        echo '</thead>';
        echo '<tbody>';
        foreach($predmeti as $predmet){
            print("<tr>\n");
            print("<td>$predmet->naziv</td><td>$predmet->espb</td><td>$predmet->obavezni</td><td>$predmet->smer</td><td>$predmet->godina</td><td><input type='checkbox' value='$predmet->id' name='check[]'/></td>\n");
            print("</tr>\n");
        }
        echo '</tbody>';
        print("</table>\n");
         print("<tr><td><input class='btn btn-success btn-lg' type='submit' name='prosledi' value='Prosledi'/></td><td></td></tr>\n");
        print("</form>\n");
        echo '</div>';
        echo '</div>';
}
                ?>
            </div>

        </div><br/>
        
        


    <script src='src/scripts.js'></script>
    <script src='src/animation.js'></script>
</body>