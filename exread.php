<?php
require 'style.html';
require './classes.php';
require './functions.php';

require("reader.php"); // php excel reader

$putanja = $_FILES['fajl_input']['tmp_name'];

$filename = $_FILES['fajl_input']['name'];
$ocenjen = false;
$ext = pathinfo($filename, PATHINFO_EXTENSION);
$izabran_predmet = $_POST['predmeti'];
$pred = vratiPredmet($izabran_predmet);

if( $ext != 'xls' ) {
       echo "<body style='background-image:url("."Images/mybg.png".");'><div class='col-xs-12 col-sm-12 col-md-12'><div class='container'><h1 class='jumbotron'>Niste ucitali excel fajl.Morate ucitati .xsl format.</h1></div></div></body>";
       echo "<script>setTimeout(()=>{window.location.href='ucitaj_excel.php'},1000)</script>";
}
else
{
    $connection=new Spreadsheet_Excel_Reader(); // our main object
    $connection->read($putanja);
    error_reporting(E_ALL & ~E_NOTICE);
    $i=1;
    $j=1;
    $pom1 = true;
    while($pom1)
    {      
        if($pom = $connection->sheets[0]["cells"][$i][$j])
        {         
            if($j == 1)
            {
                $ime = $pom;
            }
            else if($j == 2)
            {
                $indeks = $pom;
            }
            else
            {
                $ocena = $pom;
            }
            $j++;
            if($j>3)
            {
                if(!(upisi_ocenu($indeks, $ocena, $pred))) {
                    echo "<body style='background-image:url("."Images/mybg.png".");'><div class='col-xs-12 col-sm-12 col-md-12'><div class='container'><h1 class='jumbotron'>Neuspesno ste upisali ocenu za $ime.</h1></div></div></body>";
                    echo "<script>setTimeout(()=>{window.location.href='ucitaj_excel.php'},5000)</script>";
                    $ocenjen = false;
                }
                else{
                    $ocenjen = true;
                }
                $j=1;
                $i++;
            }
        }
        else
        {
            $pom1 = false;
        }
    }   
                    if($ocenjen){
                    echo "<body style='background-image:url("."Images/mybg.png".");'><div class='col-xs-12 col-sm-12 col-md-12'><div class='container'><h1 class='jumbotron'>Uspesno ste ocenili studente.</h1></div></div></body>";
                    echo "<script>setTimeout(()=>{window.location.href='ucitaj_excel.php'},5000)</script>";
                    }
}


?>