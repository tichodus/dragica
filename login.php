<?php
    require_once 'functions.php';
    session_start();
    $message = '';
    $korisnicko_ime = '';
    $sifra = '';
    if(isset($_POST['login'])){
        if($_POST['korisnicko_ime']!=null && $_POST['sifra']!=null){
            $korisnik = login($_POST['korisnicko_ime'],$_POST['sifra']);
             $_SESSION['korisnik'] = $korisnik;
            if(!$korisnik)
                $message = 'Pogresno korisnicko ime ili sifra';
            else
            {
                if(isStudent($_POST['korisnicko_ime'],$_POST['sifra']))
                echo "<script>window.location.href='student.php'</script>";
                else
                if(isProfessor($_POST['korisnicko_ime'],$_POST['sifra']))
                echo "<script>window.location.href='profesor.php'</script>";
                else
                if(isDragica($_POST['korisnicko_ime'],$_POST['sifra']))
                echo "<script>window.location.href='admin.php'</script>";
            }
    }
    else{
         
        if(empty($_POST['korisnicko_ime']))
            $korisnicko_ime = 'Polje ne sme biti prazno';
        if(empty($_POST['sifra']))
            $sifra = 'Polje ne sme biti prazno';
            
    }
        
    }
    
?>


<html>
        <head>
        <title>Dragica</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.css">
        <link rel="stylesheet" type="text/css" href="style/bootstrap-3.3.7-dist\css\bootstrap.min.css">
        <script src='src/jquery-3.1.1.min.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.js'></script>
        <script src='style/bootstrap-3.3.7-dist\js\bootstrap.min.js'></script>
        </head>

        <body class='overflow-class'>
            <div id='container' >
                 <div  class='col-xs-0 col-sm-8 col-md-8 login-img' >
                    <img src='Images/login.jpg' class ='' style='width:100%; height:inherit; background-size: cover;' />
                 </div>
                 <div id='login-container-parent' class='col-xs-12 col-sm-4 col-md-4' >
                     <div  class='col-xs-12 col-sm-10 col-md-10 '>
                        <img src='Images/elfak.png'  id='logo-elfak' class='img-responsive' style='margin:15%;' >
                     </div>
                     <div id='login-container'>
                         
                        <form action ='' method = 'post'>
                            <label for="usr">Korisnicko ime:</label>
                            <input type="text" maxLength='15' placeholder="<?php echo $korisnicko_ime;?>"   id='broj_indeksa' name='korisnicko_ime' class="form-control" id="usr" >
                            <label for="usr">Sifra:</label>
                            <input type="password"   name='sifra' placeholder="<?php echo $sifra;?>" class="form-control" id="usr" ><br/>
                            <label><?php echo $message ?></label>

                            <input type='submit' value='Uloguj se' class='form-control' name='login' style='background-color:lightgreen;'>
                        </form>
                      </div>
                    </div>
                 </div>
            
        </body>

</html>
        